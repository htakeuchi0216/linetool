<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>
		<?php echo __('SNS自動連携投稿ツール:'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<?php echo $this->Html->css('bootstrap.min'); ?>
	<style>
	body {
		padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
	}

        .message {
          background-color: #fcf8e3;
          border: 1px solid #fbeed5;
          border-radius: 4px;
          padding: 4px;
          text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
          color: #c09853;
        }
        input.form-error {
          background-color: #ffcdcd;
        }
        .error-message {
          color: red;
        }

        table.border {
          border:2px solid #000000;
          border-collapse:collapse;
        }
        .border td{
          border:1px solid #000000;
        }
        table tr:nth-child(2n+1) {  /* 奇数行 */
           border-top:2px solid #c8c8cc;
        }
        td.line_area{
           vertical-align: middle;
        }

 //     tr:nth-child(even) { background-color:#F0F0F6; }
 //    tr:nth-child(odd) { background-color:#FCFCFC; }
	</style>
	<?php echo $this->Html->css('bootstrap-responsive.min'); ?>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<!--
	<link rel="shortcut icon" href="/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">
	-->
	<?php
	echo $this->fetch('meta');
	echo $this->fetch('css');
	?>
</head>

<body>

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="/"><?php echo __('SNS自動連携投稿ツール'); ?></a>
        <div class="nav-collapse">
          <?php if ($user){
                echo "ログインユーザー：{$user['user_display_name']}";
                echo '<p><a href="/users/logout">ログアウト</a></p>';
                
                } ?>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <?php if ($user): ?>
      <div id="sidebar" class="span3">
        <ul class="nav nav-list well">

          <li class="active"><a href="/">Home</a></li>
          <li class="nav-header">メニュー</li>
<?php
if($tmp = @$social_setting['SocialSetting']){
  if( array_key_exists('line_login_id',$tmp) && $tmp['line_login_id'] ){
    if( array_key_exists('line_login_password',$tmp) && $tmp['line_login_password'] && $tmp['line_status'] == 1){
       echo '<li><a href="/socials">アカウント管理</a>';
     }
  }
}
?>

</li>
          <li><a href="/users/setting">API管理</a></li>
          <li><a href="/users/edit">設定変更</a></li>
          <?php if ($user['is_admin'] == 1): ?>
          <li class="nav-header">管理者のみ</li>
          <li><a href="/admin/user">ユーザー管理</a></li>
          <li><a href="/proxy_servers/">Proxy管理</a></li>
    
          <!--
          <li class="nav-header">ソーシャル</li>
          <li><a href="/socials/post_list">投稿データ一覧(確認用)</a></li>
          -->
          <!--
             <li><a href="/socials/get_posts">投稿データ取得(XXX通常はバッチ処理)</a></li>
          <li><a href="/socials/post_sample">LINE投稿対象記事(XXX)</a></li>
          -->
          <!-- <li><a href="/files/capture">capture</a></li> -->
          <!--
             <li class="nav-header">samples</li>
          <li><a href="/socials/twitter_sample">sample tw</a></li>
          <li><a href="/js/facebook_sample.json">sample fb</a></li
                                                                -->
             <?php endif; ?>
          <li><br><a href="/users/logout">ログアウト</a></li>

        </ul>
      </div><!-- /span3 -->
<?php endif; ?>

      <div class="span9">
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>

      </div><!-- /span9 -->

    </div><!-- /row -->

  </div> <!-- /container -->

  <!-- Le javascript  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  <script type="text/javascript">
    $('form').on('submit', function () {
      $(this).find('input:submit').attr('disabled', 'disabled').val('送信中...');
    });
  </script>  
  <?php echo $this->Html->script('bootstrap.min'); ?>
  <?php echo $this->fetch('script'); ?>
  <?php echo $this->element('sql_dump'); ?>

</body>
</html>
