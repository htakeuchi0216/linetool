<?php echo $this->Html->scriptStart( array( 'inline' => false)); ?>
$("input:button").click(function () {
  alert('update status');
  id = $(this).attr("id");　// idの取得
　val = $(this).attr("value");　// valueの取得
});
<?php echo $this->Html->scriptEnd(); ?>
<div class="users form">

<!--
最大登録件数:<?php echo $user['line_account_limit'] ?>
-->

<?php echo $this->Form->create('SocialAccount'); ?>
<fieldset>
  <legend><?php echo __('アカウント管理画面'); ?></legend>

<?php
  for($i =0 ; $i<$user['line_account_limit'] ;$i++){
    $fb_flg = false;$tw_flg = false;$ln_flg = false;
    if( isset($accounts[$i]) ){
      $tmp          =  $accounts[$i]['SocialAccount'] ;
      $tmp_facebook =  $tmp['facebook_account_id'] ;
      $tmp_twitter  =  $tmp['twitter_account_id'] ;
      $tmp_line     =  $tmp['line_account_id'] ;
      if($tmp_facebook){ $fb_flg = true; }
      if($tmp_twitter) { $tw_flg = true; }
      if($tmp_line){ $ln_flg = true; }
    }
  }
    if( $tw_flg && !$status['twitter_available'] ) {
      echo ' <div class="alert alert-notice">TwitterAPI設定が行われていません</div>';
    }
    if(isset($status['is_line_account_input'])){
      if( $ln_flg && !$status['is_line_account_input']){
        echo ' <div class="alert alert-notice">LINE@アカウント設定が行われていません</div>';
      }    
    }

    if(isset($status['line_status']) ){
      switch ($status['line_status']) {
#      case 0:
#          echo ' <div class="alert alert-notice">LINE@アカウント設定が行われていません</div>';
#          break;
      case 1:
          //echo ' <div class="alert alert-notice">LINE@へのログイン成功しました</div>';
          break;
      case 2:
          echo ' <div class="alert alert-notice">LINE@へのログインに失敗しています。ログイン設定をご確認ください</div>';
          break;
      case 3:
         if( !$status['data']['line_confirmation_code'] || $status['data']['line_confirmation_code'] == ''){
          echo ' <div class="alert alert-notice">LINE@から送られているメールをご確認いただき、認証コードをAPI管理から入力してください.</div>';
          }
          break;
#      case 9:
#          echo ' <div class="alert alert-notice">LINEアカウントがロックされています.</div>';
#          break;
#
      }
    }
 ?>

    <table class="table">
           <?php for($i =0 ; $i<$user['line_account_limit'] ;$i++): ?>
            <?php
                if( isset($accounts[$i]) ){
                 $tmp          =  $accounts[$i]['SocialAccount'] ;
                 $tmp_id       =  $tmp['id'] ;

                 $tmp_facebook =  $tmp['facebook_account_id'] ;
                 $tmp_fb_num_id =  $tmp['facebook_num_id'] ;
                 $tmp_fb_token =  $tmp['facebook_token'];
                 $tmp_twitter  =  $tmp['twitter_account_id'] ;
                 $tmp_line     =  $tmp['line_account_id'] ;
                 $tmp_fb_is_ok =  $tmp['is_facebook_available'] ;
                 $tmp_tw_is_ok =  $tmp['is_twitter_available'] ;
                 $tmp_ln_is_ok =  $tmp['is_line_available'] ;
                 $tmp_fb_name  =  $tmp["facebook_display_name"];
                }else{
                  $tmp = array();
                  $tmp_id       =  '';
                  $tmp_facebook =  '';
                  $tmp_fb_num_id =  ''; 
                  $tmp_fb_token =  ''; 
                  $tmp_twitter  =  '';
                  $tmp_line     =  '';
                  $tmp_fb_is_ok =  0;
                  $tmp_tw_is_ok =  0;
                  $tmp_ln_is_ok =  0;
                  $tmp_fb_name  =  0;
                }
            ?>
            <tr>
             <td>
              <h4>Twitter</h4>

               <?php echo $this->Form->input("SocialAccounts.$i.twitter_account_id", array('type'=>'text'  ,'label' => '', 'default'=> $tmp_twitter  )); ?>
               <p>
                   ※Twitterの以下XXXXの部分を入力してください<br>
               https://twitter.com/XXXX
                </p>
             </td>
             <td>
               <?php if($tmp_tw_is_ok){ ?>
                   <a href="/socials/change_available/<?php echo $tmp_id ?>/tw"><img src="/img/arrow.png"/ style="height:40px;"></a>
               <?php  }else{  ?>
                   <a href="/socials/change_available/<?php echo $tmp_id ?>/tw"><img src="/img/xmark.gif"/ style="height:40px;"></a>
               <?php  } ?>

             </td>
             <td rowspan="2"  class="line_area">
              <h4>LINE@</h4>

               <?php echo $this->Form->input("SocialAccounts.$i.line_account_id", array('type'=>'text' , 'label' => '','default'=> $tmp_line )); ?>
               ※LINE@MANAGERの以下XXXXの部分を入力してください<br>
               https://admin-official.line.me/XXXX/
               <br>

               <?php echo $this->Form->hidden("SocialAccounts.$i.id",array('value' => $tmp_id )); ?>
               <?php echo $this->Form->hidden("SocialAccounts.$i.user_id",array('value' => $user['id'] )); ?>
             </td>
           </tr>
           <tr>
            <td>
              <h4>Facebook</h4>
           <?php if( Configure::read("Facebook.get_from_rss")) { ?><!-- RSS ver -->
               <?php echo $this->Form->input("SocialAccounts.$i.facebook_account_id", array('type'=>'text'  ,'label' => '', 'default'=> $tmp_facebook  )); ?>
               取得元RSS:
               <a href="https://www.facebook.com/feeds/page.php?id=<?php echo $tmp_fb_num_id; ?>&format=rss20" target="_blank"><?php echo $tmp_fb_num_id; ?></a>
               <p style="font-size:small;">Facebookの以下XXXXを入力してください<br>
               https://www.facebook.com/XXXX</p>
           <?php }else{ ?><!-- /RSS ver | API ver -->

            <?php if( $tmp_fb_token){ ?>
                <?php echo $this->Form->input("SocialAccounts.$i.facebook_account_id", array('type'=>'text'  ,'label' => '', 'default'=> $tmp_facebook  )); ?>
               連携させたいユーザID<br/>またはFacebookページIDを入力してください<br>
               <?php if( $tmp_facebook ){ ?>
                 <a href="https://www.facebook.com/<?php echo $tmp_facebook; ?>" target="_blank">対象ページ</a><br/>
<!--
                 <a href="https://graph.facebook.com/v2.3/<?php echo $tmp_facebook ?>/posts?access_token=<?php echo $tmp_fb_token; ?>" target="_blank">投稿情報取得元データ<?php echo $tmp_fb_num_id; ?></a><br>
-->
                 <span><a href="/socials/fb_reset/<?php echo $tmp_id ?>">FacebookAccessTokenリセット</a></span>
                <?php } ?>
              <?php }else{ ?>
               <?php echo $this->Form->input("SocialAccounts.$i.facebook_account_id", array('type'=>'text'  ,'label' => '', 'disabled' => 'disabled' )); ?>
               <p>Facebook連携を利用するには、最初に「Facebook認証」をクリックしてください</p>
              <a href="/auth/facebook/?id=<?php echo $tmp_id ?>">Facebook認証</a><br>
              <?php } ?>

           <?php } ?>
<br>
            </td>
            <td>
               <?php if($tmp_fb_is_ok){ ?>
                   <a href="/socials/change_available/<?php echo $tmp_id ?>/fb"><img src="/img/arrow.png"/ style="height:40px;"></a>
               <?php  }else{  ?>
                   <a href="/socials/change_available/<?php echo $tmp_id ?>/fb"><img src="/img/xmark.gif"/ style="height:40px;"></a>
               <?php  } ?>
            </td>

          </tr>
        <?php endfor; ?>
            </table>
</fieldset>
<?php echo $this->Form->end(__('登録')); ?>
</div>
