<div class="socialAccounts view">
<h2><?php echo __('Social Account'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Id'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seq'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['seq']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Line Account Id'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['line_account_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Twitter Account Id'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['twitter_account_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Facebook Account Id'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['facebook_account_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Available'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['is_available']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Deleted'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['is_deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($socialAccount['SocialAccount']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Social Account'), array('action' => 'edit', $socialAccount['SocialAccount']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Social Account'), array('action' => 'delete', $socialAccount['SocialAccount']['id']), array(), __('Are you sure you want to delete # %s?', $socialAccount['SocialAccount']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Social Accounts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Social Account'), array('action' => 'add')); ?> </li>
	</ul>
</div>
