<div class="socialAccounts form">
<?php echo $this->Form->create('SocialAccount'); ?>
	<fieldset>
		<legend><?php echo __('Add Social Account'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('seq');
		echo $this->Form->input('line_account_id');
		echo $this->Form->input('twitter_account_id');
		echo $this->Form->input('facebook_account_id');
		echo $this->Form->input('is_available');
		echo $this->Form->input('is_deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Social Accounts'), array('action' => 'index')); ?></li>
	</ul>
</div>
