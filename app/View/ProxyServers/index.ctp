<div class="proxyServers index">
	<h2><?php echo __('Proxy管理'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id',"id"); ?></th>
			<th><?php echo $this->Paginator->sort('ip'); ?></th>
			<th><?php echo $this->Paginator->sort('port'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('area'); ?></th>
			<th><?php echo $this->Paginator->sort('postcount',"投稿回数"); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('disabled_date',"無効日"); ?></th>
			<th><?php echo $this->Paginator->sort('created',"作成日"); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($proxyServers as $proxyServer): ?>
	<tr>
		<td><?php echo h($proxyServer['ProxyServer']['id']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['ip']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['port']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['username']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['area']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['postcount']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['status']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['disabled_date']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['created']); ?>&nbsp;</td>
		<td><?php echo h($proxyServer['ProxyServer']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $proxyServer['ProxyServer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $proxyServer['ProxyServer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $proxyServer['ProxyServer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $proxyServer['ProxyServer']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('{:count}件中{:start}-{:end}件表示 ')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('新規登録'), array('action' => 'add')); ?></li>
	</ul>
</div>
