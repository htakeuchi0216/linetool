<div class="proxyServers view">
<h2><?php echo __('Proxy Server'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['ip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Port'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['port']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Area'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Postcount'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['postcount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disabled Date'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['disabled_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($proxyServer['ProxyServer']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proxy Server'), array('action' => 'edit', $proxyServer['ProxyServer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proxy Server'), array('action' => 'delete', $proxyServer['ProxyServer']['id']), array(), __('Are you sure you want to delete # %s?', $proxyServer['ProxyServer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proxy Servers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proxy Server'), array('action' => 'add')); ?> </li>
	</ul>
</div>
