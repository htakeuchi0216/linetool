<div class="proxyServers form">
<?php echo $this->Form->create('ProxyServer'); ?>
	<fieldset>
		<legend><?php echo __('Edit Proxy Server'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('ip');
		echo $this->Form->input('port');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('area');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProxyServer.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProxyServer.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Proxy Servers'), array('action' => 'index')); ?></li>
	</ul>
</div>
