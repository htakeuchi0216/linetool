<div class="proxyServers form">
<?php echo $this->Form->create('ProxyServer'); ?>
	<fieldset>
		<legend><?php echo __('Proxy新規登録'); ?></legend>
	<?php
		echo $this->Form->input('ip');
		echo $this->Form->input('port');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('area');
	?>
	</fieldset>
<?php echo $this->Form->end(__('新規登録')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Proxy Servers'), array('action' => 'index')); ?></li>
	</ul>
</div>
