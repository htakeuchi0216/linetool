
<h1>新規登録</h1>

<?php print(
  $this->Form->create('User', array('novalidate' => true)) .
  $this->Form->input('username', array('label' => 'ユーザーID') ) .
  $this->Form->input('password', array('label' => 'パスワード') ) .
  $this->Form->input('password_confirm', array('type' => 'password' ,'label' => 'パスワード(再入力)','required' => true,)) .
  $this->Form->input('user_display_name', array('label' => '名前') ) .
  $this->Form->input('email', array('label' => 'メールアドレス') ) .
# $this->Form->input('is_admin', array('type' => 'checkbox', 'label' => '管理者フラグ') ) .
  $this->Form->end('登録')
); ?>