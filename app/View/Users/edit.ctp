<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('ユーザー情報変更'); ?></legend>
         ユーザーID:
	<?php
		echo $user['username'];
		echo $this->Form->input('id' );
		echo $this->Form->input('user_display_name', array( 'label' => '名前'  ) );
		echo $this->Form->input('email', array( 'label'=>'メールアドレス'  ));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('更新')); ?>

	<fieldset>
		<legend><?php echo __('パスワード変更'); ?></legend>

		<?php echo $this->Form->create('User', array('url' => '/users/update_password')); ?> 
		<?php print(
		$this->Form->input('id' ) .
		$this->Form->input('password', 
		   array('label' =>  'パスワード', 'value'=> '') ) .
		$this->Form->input('password_confirm', array('type' => 'password' ,'label' => 'パスワード(再入力)','required' => true,)) .
		$this->Form->end('パスワード変更')
		); ?>
	</fieldset>
</div>
