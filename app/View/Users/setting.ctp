<div class="setting form">
<?php echo $this->Form->create('SocialSetting', array('novalidate' => true)); ?>
	<fieldset>
		<legend><?php echo __('API管理'); ?></legend>
	<?php
		echo $this->Form->input('id' );
               if( Configure::read("Facebook.get_from_rss")){
               }else{
		//echo $this->Form->input('facebook_access_token' , array( 'label' => 'Facebookアクセストークン') );
		//echo $this->Form->input('facebook_api_key'      , array( 'label' => 'FacebookAPIキー'));
		//echo $this->Form->input('facebook_api_secret' , array( 'label' => 'Facebookシークレットキー'));
               }

		echo $this->Form->input('twitter_consumer_key'        , array( 'label' => 'Twitter Consumer Key'        , 'style' => 'width:240px' ));
		echo $this->Form->input('twitter_consumer_secret'     , array( 'label' => 'Twitter Consumer Secret'     , 'style' => 'width:440px'));
		echo $this->Form->input('twitter_access_token'        , array( 'label' => 'Twitter Access Token'        , 'style' => 'width:440px'));
		echo $this->Form->input('twitter_access_token_secret' , array( 'label' => 'Twitter Access Token Secret' , 'style' => 'width:440px'));
		echo "<br>";

		echo $this->Form->input('line_login_id'         , array( 'type' => 'text', 'label' => 'LINE@ログインID', 'default' => '' ));
		echo $this->Form->input('line_login_password'   , array( 'type' =>'password', 'label' => 'LINE@ログインパスワード' , 'default' => ''));
		if( $social_setting && $social_setting['SocialSetting']['line_status'] == 3 ){
                  echo $this->Form->input('line_confirmation_code'   , array( 'type' =>'text', 'label' => 'LINE公式アカウント管理画面:認証コード'));
                } 
	?>

        <?php echo $this->Form->hidden("SocialSetting.user_id",array('value' => $user_id )); ?>
        <?php echo $this->Form->hidden("SocialSetting.line_status"); ?>

	</fieldset>
<?php echo $this->Form->end(__('更新')); ?>
</div>
