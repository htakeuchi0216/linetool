
<?php echo $this->Html->scriptStart( array( 'inline' => false)); ?>
//alert( "alert");
$("input:button").click(function () {
  id = $(this).attr("id");　// idの取得
　val = $(this).attr("value");　// valueの取得
  if(id != undefined){
    $("#EditUserForm").attr({"action":"/admin/delete/"+id, "method":"post"});

    if( confirm( "ID:" + id + "のユーザーを削除します。よろしいですか？")){
      $("#EditUserForm").submit();
    }
    else{
    }

  }

});
<?php echo $this->Html->scriptEnd(); ?>

<h1>ユーザー管理</h1>

<table class="table">
<tr>
   <th>id</th>
   <th>ユーザーID</th>
   <th>名前</th>
   <th>メールアドレス</th>
   <th>登録数</th>
   <th>is_admin</th>
   <th>削除 </th>
</tr>

<?php echo $this->Form->create('Edit'); ?>
<?php for($i =0 ; $i<count($users) ;$i++): ?>
<?php $user = $users[$i]; ?>
<tr>  
   <td> <?php echo h($user['User']['id']); ?></td>
   <td> <?php echo h($user['User']['username']); ?></td>
   <td> <?php echo h($user['User']['user_display_name']); ?></td>
   <td> <?php echo h($user['User']['email']); ?></td>
   <td>
     <?php echo $this->Form->hidden("User.$i.id",array('value' => $user['User']['id'] )); ?>
     <?php echo $this->Form->input("User.$i.line_account_limit",
              array('label'=> '' ,
                    'type'=>'text',
                    'size'=>'3',
                    'maxlength' => 2,
                    'style'=> 'width:50px;',
                    'default'=> $user['User']['line_account_limit'] ));
         ?>
   </td>
   <td> <?php echo h($user['User']['is_admin']); ?></td>
   <td>
        <?php echo $this->Form->hidden("User.id",array('value' => $user['User']['id'] )); ?>

    <input type="button" class="delete" id="<?php echo $user['User']['id'] ?>" value="削除"  />

</td>
   </tr>

 <?php endfor; ?>
</table>
	

<?php 
$options = array(
    'label' => '登録',
    'class' => 'btn btn-info',
);
print $this->Form->end($options); ?>
<br>
<a href="/admin/download_csv" target="_blank" class="btn btn-info" role="button">csv出力</a>
