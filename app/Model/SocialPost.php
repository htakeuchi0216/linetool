<?php
App::uses('AppModel', 'Model');
/**
 * SocialPost Model
 *
 * @property SocialAccount $SocialAccount
 * @property Account $Account
 */
class SocialPost extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'social_account_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'account_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'post_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'body' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SocialAccount' => array(
			'className' => 'SocialAccount',
			'foreignKey' => 'social_account_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	function _get_max_post_id($social_account_id,$account_id,$type=1){
	  $result = $this->find(
	    'first',
	    array(
	      "fields" => "MAX(SocialPost.post_id) as max_id",
	      'conditions' => array(
		'SocialPost.type' => $type,
		'SocialPost.social_account_id' => $social_account_id,
		'SocialPost.account_id'        => $account_id
	      ),
	    )
	  );
	  $max_id = null;
	  if(isset($result[0]['max_id'])){
	    $max_id = $result[0]['max_id'];
	  }
	  return $max_id;
	}

	function get_facebook_max_post_id($social_account_id,$account_id){
	  return $this->_get_max_post_id($social_account_id,$account_id,2);
	}

	function get_twitter_max_post_id($social_account_id,$screen_name){
	  return $this->_get_max_post_id($social_account_id,$screen_name,1);
	}

	function get_saved_post_ids($social_account_id,$type=1){
	  $result = $this->find(
	    'all',
	    array(
	      "fields" => "SocialPost.post_id",
	      'conditions' => array(
		'SocialPost.type' => $type,
		'SocialPost.social_account_id' => $social_account_id,
	      ),
	    )
	  );
	  //debug($result);
	  $ids = array();
	  foreach( $result as $r ){
	    $ids[] = $r['SocialPost']['post_id'];
	  }
	  return $ids;
	}
    function get_have_img_posts(){
      $sql = "SELECT id,img_url,type,original_posted
              FROM social_posts
              WHERE img_url is not NULL
                    AND img_url !=''
                    AND img_download_flg = 0 
              ORDER BY id;";
      $params = array();
      $data = $this->query($sql,$params);
      return $data;
    }
  function get_post_data($social_account_id,$account_id,$type,$datetime){
    $target_posts = $this->find(
      'all',
      array('conditions' => array(
                                  "SocialPost.social_account_id"   => $social_account_id,
                                  "SocialPost.account_id"          => $account_id,
                                  "SocialPost.type"                => $type,
                                  "SocialPost.original_posted >="  => $datetime,
                                  "SocialPost.is_posted" => 0,
                                  )
            )
                                );
    return $target_posts;
  }

}
