<?php
App::uses('AppModel', 'Model');


class User extends AppModel {
  //入力チェック機能
  public $validate = array(
    'username' => array(
      array(
	'rule' => 'isUnique', //重複禁止
	'message' => '既に使用されているユーザーIDです。'
      ),
      array(
	'rule' => 'alphaNumeric', //半角英数字のみ
	'message' => 'ユーザーIDは半角英数字にしてください。'
      ),
      array(
	'rule' => array('between', 2, 32), //2～32文字
	'message' => 'ユーザーIDは2文字以上32文字以内にしてください。'
      )
    ),
    'password' => array(
      array(#XXX
	'rule' => array( 'confirmPassword', 'password_confirm'),
	'message' => 'パスワードが一致しません'
      ),
      array(
	'rule' => 'alphaNumeric',
	'message' => 'パスワードは半角英数字にしてください。'
      ),
      array(
	'rule' => array('between', 8, 32),
	'message' => 'パスワードは8文字以上32文字以内にしてください。'
      )
    ),
    'user_display_name' => array(
      array(
	'rule' => 'notEmpty',
	'message' => '名前を入力してください。'
      ),
    ),
    'email' => array(
      array(
	'rule' => 'notEmpty',
	'message' => 'メールアドレスを入力してください。'
      ),
    )
  );
  public function beforeSave($options = array()) {
    if( isset($this->data['User']['password']) ){
      $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
    }
    return true;
  }

  public function getAvailables(){
    return $this->find(
      'all',
      array(
	'conditions' => array(
	  'User.is_deleted' => 0
	)
      )
    );
  }
  public function confirmPassword( $field, $password_confirm) {
    if ($field['password'] === $this->data[$this->name][$password_confirm]) {
      return true;
    }
  }

}
