<?php
App::uses('AppModel', 'Model');
/**
 * ProxyServer Model
 *
 */
class ProxyServer extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'proxy_server';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'ip';

}
