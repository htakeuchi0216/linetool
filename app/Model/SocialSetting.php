<?php
App::uses('AppModel', 'Model');
/**
 * SocialSetting Model
 *
 */
class SocialSetting extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'social_setting';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
	  /*
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
*/
		'user_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'facebook_access_token' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'facebook_api_key' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'facebook_api_secret' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'twitter_access_token' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'twitter_api_key' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'twitter_api_secret' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'line_login_id' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'line_login_password' => array(
			'maxLength' => array(
			  'rule' => array('maxLength',255),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'line_status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_deleted' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'created' => array(
			'n' => array(
				'rule' => array('n'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'modified' => array(
			'n' => array(
				'rule' => array('n'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	function find_by_id($id){
	  return $this->find(
	    'first',
	    array(
	      'conditions' => array(
		'SocialSetting.id' => $id
	      )
	    )
	  );
	}

	function get_status($user_id){
	  $data = $this->find_by_user_id($user_id);
	  $status = array(
	    'line_available'     => 0,
	    'facebook_available' => 0,
	    'twitter_available'  => 0,
	    'line_status'        => 0, // $d['line_status']
	    'is_line_account_input' =>0,
	  );
          if( array_key_exists('SocialSetting', $data) ){ 
	    $d    = $data['SocialSetting'];
            $status['line_status'] = $d['line_status'];
            if(
	      $d['twitter_consumer_key'] &&
	      $d['twitter_consumer_secret'] &&
	      $d['twitter_access_token'] &&
	      $d['twitter_access_token_secret']
	    ){
	      $status['twitter_available'] = 1;
	    }
	    if($d['line_login_id'] && $d['line_login_password']){
	      $status['is_line_account_input'] = 1;
	    }
        $status['data'] = $d;
          }
	  return $status;
	}

	function find_by_user_id($user_id){
	  return $this->find(
	    'first',
	    array(
	      'conditions' => array(
		'SocialSetting.user_id' => $user_id
	      )
	    )
	  );
	}

	function is_changed($key,$before,$new){
	  if (array_key_exists('SocialSetting', $new)){
	    if( isset($new['SocialSetting'][$key]) ){
	      if ( !$before || $before['SocialSetting'][$key] != $new['SocialSetting'][$key]){
	      //print "$key : CHANGEDXxXX";
		return true;
	      }
	    }
	  }
	  //print "$key : NOT CHANGEDXXX";
	  return false;
	}

	function getdata($key,$before,$new){
	  if( isset($new['SocialSetting'][$key]) && $new['SocialSetting'][$key] ){
	    return $new['SocialSetting'][$key];
	  }
	  if( isset($before['SocialSetting'][$key]) && $before['SocialSetting'][$key] ){
	    return $before['SocialSetting'][$key];
	  }
	  return;
	}

	function save($data){
      $this->log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ " ,LOG_DEBUG);
      $this->log("[SocialSetting] begin SocialSetting save " ,LOG_DEBUG);

	  $before = $this->find_by_id($data['SocialSetting']['id']);
	  $before = $this->find_by_id($data['SocialSetting']['id']);
	  if(!isset($data['SocialSetting']['line_status']) || !$data['SocialSetting']['line_status'] ){
	    $data['SocialSetting']['line_status']=0;
	  }

	  if( $this->is_changed("facebook_api_key",$before,$data) || $this->is_changed("facebook_api_secret",$before,$data) ){
        $this->log("[SocialSetting] facebook_api or facebook_api_secret is changed " ,LOG_DEBUG);

	    // FacebookAPIチェックXXX
	    //$this->invalidate("FacebookAPI","FB-ERRORTEST");
	    return ;
	  }
      $errmsgs = array();
      $twitter_api_is_none = false;
      #TwitterAPI情報が何も入力されていない場合
      if( !$data['SocialSetting']['twitter_consumer_key'] &&
          !$data['SocialSetting']['twitter_consumer_secret'] &&
          !$data['SocialSetting']['twitter_access_token'] &&
          !$data['SocialSetting']['twitter_access_token_secret']){
        $this->log("[SocialSetting]"."Twitterアカウントからの自動投稿機能を利用する場合、TwitterAPIキー情報を入力してください" ,LOG_DEBUG);
        $twitter_api_is_none = true;
        $errmsgs[] = array("TwitterAPI" ,"Twitterアカウントからの自動投稿機能を利用する場合、TwitterAPIキー情報を入力してください");
        #$this->invalidate("TwitterAPI" ,"Twitterアカウントからの自動投稿機能を利用する場合、TwitterAPIキー情報を入力してください");
        #return;
      }

	  if( $this->is_changed("twitter_consumer_key",$before,$data) ||
	      $this->is_changed("twitter_consumer_secret",$before,$data) ||
	      $this->is_changed("twitter_access_token",$before,$data) ||
	      $this->is_changed("twitter_access_token_secret",$before,$data) 
	  ){
        $this->log("[SocialSetting]"."Twitter API is changed" ,LOG_DEBUG);

        $_consumer_key        = $this->getdata("twitter_consumer_key"        ,$before,$data);
	    $_consumer_secret     = $this->getdata("twitter_consumer_secret"     ,$before,$data);
	    $_access_token        = $this->getdata("twitter_access_token"        ,$before,$data);
	    $_access_token_secret = $this->getdata("twitter_access_token_secret" ,$before,$data);

	    $this->log("@@@@@ TWITTER API TEST @@@@" ,LOG_DEBUG);
	    $TwitterOAuth = new TwitterOAuth($_consumer_key,$_consumer_secret,$_access_token,$_access_token_secret);
	    $json = $TwitterOAuth->OAuthRequest(
	      'https://api.twitter.com/1.1/users/show.json',
	      'GET',
	      array( "screen_name"=> "rsarver")
	    );
	    $response = json_decode($json, true);
	    if( isset($response['id']) && $response['id'] ){
	      $this->log("@@@@@ TWITTER API : SUCCESS " ,LOG_DEBUG);

	      //print "ID GET SUCCESS";
	    }
	    else{
	      # Twitter APIコールが失敗した場合
	      #それ以外の項目の保存だけをして、twitterはエラーとして返す
	      $this->log("@@@@@ TWITTER API : FAILURE " ,LOG_DEBUG);
	      unset ( $data['SocialSetting']['twitter_consumer_key']);
	      unset ( $data['SocialSetting']['twitter_consumer_secret']);
	      unset ( $data['SocialSetting']['twitter_access_token']);
	      unset ( $data['SocialSetting']['twitter_access_token_secret']);
	      #parent::save($data);

          $errmsgs[] = array("TwitterAPI" ,"TwitterAPIキーの情報が正しくありませんでした。ご確認の上、再度正しい情報を入力してください。");
          $errmsgs[] = array("twitter_consumer_key" ,"");
          $errmsgs[] = array("twitter_consumer_secret" ,"");
          $errmsgs[] = array("twitter_access_token" ,"");
          $errmsgs[] = array("twitter_access_token_secret" ,"");

	    }
	  }

      $confirm_code_is_changed = $this->is_changed("line_confirmation_code",$before,$data);
       if( $this->is_changed("line_login_id",$before,$data)
          || $this->is_changed("line_login_password",$before,$data) 
          || $confirm_code_is_changed
          ){
        # 使用可能文字のチェックXXXXX
        $this->log("[SocialSetting] line_login_account is changed" ,LOG_DEBUG);

        $user_id = $data['SocialSetting']['user_id'];
        $mail = $data['SocialSetting']['line_login_id'];
        $pass = $data['SocialSetting']['line_login_password'];
        $confirmation_code = @$data['SocialSetting']['line_confirmation_code'];
        $this->log("[SocialSetting] mail: $mail " ,LOG_DEBUG);
        $this->log("[SocialSetting] pass: $pass " ,LOG_DEBUG);
        $this->log("[SocialSetting] confirmation_code : $confirmation_code " ,LOG_DEBUG);

        if ( ($mail && !preg_match('/^[a-zA-Z0-9@._+]+$/', $mail) )
             || ($pass && !preg_match('/^[a-zA-Z0-9.@_]+$/', $pass))
             || ($confirmation_code && !preg_match('/^[a-zA-Z0-9.@_]+$/', $pass))
             ) {
          // 英数字でない場合
          $this->log("[SocialSetting]非対応文字が含まれています" ,LOG_DEBUG);

	      unset ( $data['SocialSetting']['line_login_id']);
	      unset ( $data['SocialSetting']['line_login_password']);

          $errmsgs[] = array("LINELOGIN" ,"LINE@ログインIDもしくはパスワードに非対応文字が含まれています");
          $errmsgs[] = array("line_login_id" ,"");
          $errmsgs[] = array("line_login_password" ,"");
        }
        elseif($user_id && $mail && $pass){
          $this->log("[SocialSetting] try login to line@ admin page" ,LOG_DEBUG);
          $logpath = "/var/log/linetool/login_line_from_adminpage.log";
          $logpath_err = "/var/log/linetool/login_line_from_adminpage-err.log";
          $exec_str = "sh /var/www/html/cakephp/app/login_from_admin_page.sh $user_id $mail $pass $confirmation_code >> $logpath 2>> $logpath_err";
          $this->log("[SocialSetting]EXEC : $exec_str " ,LOG_DEBUG);
          exec( $exec_str, $arr, $res );

          $this->log("[SocialSetting] res : $res" ,LOG_DEBUG);
          if ($res == "0"){ #login success
            $data['SocialSetting']['line_status'] = 1;
            $this->log("[SocialSetting]success login" ,LOG_DEBUG);
            $errmsgs[] = array("LINELOGIN" ,"LINE@へのログインに成功しました。");
          }elseif( $res == "9"){ # 認証コード送信
            $this->log("[SocialSetting]Confirm mail seneded" ,LOG_DEBUG);
            $data['SocialSetting']['line_status'] = 3;
            $errmsgs[] = array("LINELOGIN" ,"LINE@より認証コードが送信されました。メールをご確認ください。");
          }else{ # ログイン失敗
            $this->log("[SocialSetting] login failure" ,LOG_DEBUG);
            $this->log( print_r($arr,true) ,LOG_DEBUG);
            if( $confirm_code_is_changed ){
              unset ( $data['SocialSetting']['line_confirmation_code']);
              $errmsgs[] = array("LINELOGIN" ,"LINE@のログインに失敗しました。認証コードを確認してください");
            }else{
              unset ( $data['SocialSetting']['line_login_id']);
              unset ( $data['SocialSetting']['line_login_password']);
              $errmsgs[] = array("LINELOGIN" ,"LINE@のログインに失敗しました。メールアドレス、もしくはパスワードを確認してください");
              $errmsgs[] = array("line_login_id" ,"");
              $errmsgs[] = array("line_login_password" ,"");
            }
          }
          $data['SocialSetting']['line_confirmation_code'] = '';

        }
      }


      /*
      if( !$data['SocialSetting']['twitter_consumer_key'] &&
	      !$data['SocialSetting']['twitter_consumer_secret'] &&
	      !$data['SocialSetting']['twitter_access_token'] &&
	      !$data['SocialSetting']['twitter_access_token_secret'] &&
          !$data['SocialSetting']['line_login_id'] &&
          !$data['SocialSetting']['line_login_password'] ){
        return;
      }
      */

      $this->log("[SocialSetting] dump data" ,LOG_DEBUG);
      $this->log( print_r($data,true) ,LOG_DEBUG);
      $this->log("[SocialSetting][ERRMSG] ERROR" ,LOG_DEBUG);
      $this->log( print_r($errmsgs,true) ,LOG_DEBUG);
      
      $status = parent::save($data);
      $err_flg = false;
      foreach ( $errmsgs as $err){
        $this->log( "@@@@@@@@@@@@@@@@@@@@@--------" ,LOG_DEBUG);
        $this->log( print_r($err,true) ,LOG_DEBUG);
        $this->invalidate($err[0] , $err[1]);
        $this->log("[SocialSetting][ERRMSG] ". $err[0]." -- ". $err[1] ,LOG_DEBUG);
        $err_flg = true;
      }
      $this->log( " status : $status" ,LOG_DEBUG);
      $this->log( print_r($status,true) ,LOG_DEBUG);
      if(!$status){
        $this->log( "XXX parent::save return false----" ,LOG_DEBUG);
      }else{
        $this->log( "XXX parent::save return true----" ,LOG_DEBUG);
      }
      if(!$err_flg){
        return $status;
      }
      return;
	}

	public function beforeSave()
	{
	  $enc_pass = $this->encrypt( @$this->data['SocialSetting']['line_login_password'] );
	  $this->data['SocialSetting']['line_login_password'] = $enc_pass;
	  return true;
	}
	public function afterFind($results, $primary = false) {
	  $new_results = array();
	  foreach ($results as $r) {
	    $r['SocialSetting']['line_login_password'] =  $this->decrypt($r['SocialSetting']['line_login_password']);
	    $new_results[] = $r;
	  }
	  return $new_results;
	}

	public function encrypt($text) {
	  return base64_encode(Security::rijndael($text, Configure::read('Security.db.key'), 'encrypt'));
	}
	public function decrypt($text) {
	  return Security::rijndael(base64_decode($text), Configure::read('Security.db.key'), 'decrypt');
	}
}
