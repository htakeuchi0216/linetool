<?php
App::uses('AppModel', 'Model');
//App::import('Component', 'SocialCommon');

App::uses('ComponentCollection', 'Controller');
App::uses('SocialCommonComponent', 'Controller/Component');

/**
 * SocialAccount Model
 *
 */
class SocialAccount extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
  public $validate = array(
    'user_id' => array(
      'notEmpty' => array(
                          'rule' => array('notEmpty'),
                          //'message' => 'Your custom message here',
                          //'allowEmpty' => false,
                          //'required' => false,
                          //'last' => false, // Stop validation after this rule
                          //'on' => 'create', // Limit validation to 'create' or 'update' operations
                          ),
                       ),
    'seq' => array(
                   'numeric' => array(
                                      'rule' => array('numeric'),
                                      //'message' => 'Your custom message here',
                                      //'allowEmpty' => false,
                                      //'required' => false,
                                      //'last' => false, // Stop validation after this rule
                                      //'on' => 'create', // Limit validation to 'create' or 'update' operations
                                      ),
                   ),
    //'facebook_account_id' => array(
    //  'rule'    => array( 'isAvailableFacebookAccountId'),
    //  'message' => 'Facebookアカウントが有効でありません'
    //),
    /*
      'line_account_id' => array(
      'notEmpty' => array(
      //'rule' => array('notEmpty'),
      //'message' => 'Your custom message here',
      //'allowEmpty' => false,
      //'required' => false,
      //'last' => false, // Stop validation after this rule
      //'on' => 'create', // Limit validation to 'create' or 'update' operations
      ),
      ),
    */
  );
  /*
    function isAvailableFacebookAccountId($check){
    print "@@@isAvailableFacebookAccountId@@@<br/>";
    $result = false;
    debug($this->data);
    debug($check);
    print $this->data['SocialAccount']['facebook_account_id'];
    if( $check['facebook_account_id'] ){
    print $this->data['SocialAccount']['facebook_account_id']."<BR/>";
    print $check['facebook_account_id']."<BR/>";
    
    if($this->data['SocialAccount']['facebook_account_id'] != $check['facebook_account_id'] ){
    print "CHANGEDXXX";
    exit();
    }
    }
    return $result;
    }
  */
  function find_by_id($id){
    return $this->find(
      'first',
      array(
            'conditions' => array(
                                  'SocialAccount.id' => $id
                                  )
            )
                       );
  }

  function save($data){
    $collection = new ComponentCollection();
    $SocialCommon = new SocialCommonComponent($collection);
    $user_id = $data['user_id'];
    $before = array();
    if(isset($data['id']) ){
      $before = $this->find_by_id($data['id']);
    }

    $now = date('Y-m-d H:i:s');
    foreach ( array('facebook','twitter','line') as $sv){
      print $sv ."\n";
      $keyname = $sv.'_account_id';
      /* アカウント変更時if*/

      $before_account_id = "";
      if( isset($before['SocialAccount'][$keyname]) ){
        $before_account_id = $before['SocialAccount'][$keyname];
      }

      if( isset($data[$keyname]) && $before_account_id != $data[$keyname]){
        $new_id = $data[$keyname];
        
        // Twitter : 存在するIDかチェックをする
        if ($sv == 'twitter' && $new_id){
          $tw_res = $SocialCommon->twitter_user_check($new_id,$user_id);
          if($tw_res == "TW-ERR1"){
            $this->invalidate('twitter_account_id','Twitterアカウントを登録する場合は、先にAPI管理画面でTwitterのAPIキー情報を登録してください');
            return ;
          }elseif($tw_res == "TW-ERR2"){
            $this->invalidate('twitter_account_id','TwitterのAPIキーが凍結されています。API管理画面で新しくAPIキー情報を登録し直してください');
            return ;
          }elseif($tw_res == "TW-ERR3"){
            $this->invalidate('twitter_account_id','twitter アカウントの確認に失敗しました');
            return ;
          }

        }
        //RSSから情報取得の場合のFacebookが更新された際には、
        //アカウントの存在チェックを行う
        if( $sv == 'facebook' && $new_id ){
          $this->log("user_id:$user_id -- facebook_id is changed");
          $this->log("$before_account_id => $new_id ");

          if( Configure::read('Facebook.get_from_rss') ){
            $fb_name = $new_id;
            $fb_id = $SocialCommon->get_facebook_account_id($fb_name);
            if(!$fb_id){
              $this->invalidate('twitter_account_id','Facebookアカウントの確認に失敗しました');
              return;
            }
            $data['facebook_num_id'] = $fb_id;
          }
          else{# API使用時
            $this->log(" account check by using api ");
            $token = "";
            if( isset($before['SocialAccount']) &&
                      isset($before['SocialAccount']['facebook_token'])
                ){
              $token = $before['SocialAccount']['facebook_token'];
            }
            if(!$token && isset($data['facebook_token']) ){
              $token = $data['facebook_token'];
            }
            $url = "https://graph.facebook.com/v2.3/{$new_id}/feed?access_token={$token}";
            $json = @file_get_contents($url);
            $this->log("$url ");
            if($json){
              $this->log("get json success");
            }
            else{
              $this->log("get json ERROR[acebookアカウントの確認に失敗しました]");
              $this->invalidate('twitter_account_id','Facebookアカウントの確認に失敗しました');
              return;
            }

          }
        }

        // TODO lineのアカウントチェック
        // XXX
      }
      /* アカウント変更時endif*/

      /* flg変更時*/
      //有効フラグの変更状況によって有効日時を変更
      $flg_key = "is_{$sv}_available";
      if( isset($data[$flg_key]) && $before['SocialAccount'][$flg_key] != $data[$flg_key]){
        if($data[$flg_key]){
          $data[$sv.'_available_datetime' ] = $now;
        }else{
          $data[$sv.'_available_datetime' ] = null;
        }
      }
      
      // account_idが空に変更された場合、有効日時も空にする
      if(isset($data[$keyname]) && !$data[$keyname]){
        $data[$sv.'_available_datetime' ] = null;
        if( $sv == 'facebook' ){
          $data['facebook_num_id'] = null;
        }
      }

    }
    return parent::save($data);
  }

  public function get_by_user_id($user_id){
    return $this->find(
                       'all',
                       array(
                             'conditions' => array(
                                                   'SocialAccount.user_id' => $user_id
                                                   ),
                             'order' => 'SocialAccount.id'
                             )
                       );
  }
}
