# -*- coding: utf-8 -*-
describe "JavaScript を使ったページのテスト", js: true do
  it "JavaScript が実行されたかのテスト" do
    # テスト内容を書く
    @empty_array = []
    @empty_array.size.should == 0

    url = 'http://google.co.jp/'
    visit url
    expect(page).to have_content 'Google'
  end
end

