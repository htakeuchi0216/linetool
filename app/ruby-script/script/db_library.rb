# coding: utf-8

# proxy関連
def get_proxy_by_random client
  p "[db]get_proxy_by_random "
  sql = "SELECT * FROM proxy_server AS tbl,
  ( SELECT id FROM proxy_server WHERE status=0 ORDER BY RAND() LIMIT 0 , 1) AS randam
  WHERE tbl.id = randam.id LIMIT 0 , 1"
  client.query(sql).each do | col|
    return col
  end

end

def get_available_proxy_count client
  p "[db]get_available_proxy_count "
  sql = "SELECT count(*) as cnt FROM proxy_server WHERE status = 0 "
  client.query(sql).each do | col|
    return col["cnt"]
  end
end

def increment_proxy_post_count client,id
  p "[db]increment_proxy_post_count"
  client.query("update proxy_server SET postcount=postcount+1 ,modified =now()  WHERE id='#{id}'")
end

def proxy_disable client,id,ip
  p "[db] proxy_disable"
  client.query("update proxy_server SET status=1 ,disabled_date =now() WHERE id='#{id}'")

  remain_cnt = get_available_proxy_count client
  p " proxy remain : #{remain_cnt} "
  send_proxy_disable_mail(ip)
  # 残り5件以下の場合アラート用メールも送信する
  if remain_cnt <= 5
    send_proxy_alert_mail(remain_cnt,ip)
  end
end

def send_mail(subject,body)
  mail = Mail.new do
    from    "tohaya0216@gmail.com"
    to      "imuta@flmk.jp"
    cc      "tohaya0216@gmail.com"
    subject subject
    body    body
  end
  #mail.delivery_method(:smtp,
  #  address:        "example.co.jp",
  #  port:           587,
  #  domain:         "example.co.jp",
  #  authentication: :login,
  #  user_name:      "<user_name>",
  #  password:       "<password>"
  #)
  mail.deliver
end

def send_proxy_disable_mail(ip)
  body = "ip:#{ip} のプロキシが制限されました

http://sns-autopost.com/proxy_servers/"

  send_mail("プロキシ制限通知(ip:#{ip})",body)
end

def send_proxy_alert_mail(remain_cnt,ip)
  body = "ip:#{ip} のプロキシが制限されました
残り#{remain_cnt}件です。

http://sns-autopost.com/proxy_servers/
お早目にご確認ください。"

  send_mail("プロキシ制限通知(残り#{remain_cnt}件)",body)
end


#
def post_complete client,id 
  p "[db] post_complete"
  client.query("update social_posts SET is_posted=1 ,line_posted =now() WHERE id='#{id}'")
end

def line_login_success( client, user_id )
  p "[db] line_login_success"
  client.query("update social_setting SET line_status = 1,line_confirmation_code = '',line_login_ng_time=null  , modified = now() WHERE user_id='#{user_id}'")
end

def line_login_failure( client, user_id )
  p "[db] line_login_failure"
  client.query("update social_setting
               SET line_status = 2,line_confirmation_code = '', line_login_ng_time=now()  ,modified = now() WHERE user_id='#{user_id}'")
end

def confirmation_mail_is_sended( client, user_id )
  p "[db] confirmation_mail_is_sended #{user_id} "
  client.query("update social_setting
               SET line_status = 3,line_confirmation_code = '', line_login_ng_time=now()  ,modified = now() WHERE user_id='#{user_id}'")
end

def line_account_has_been_locked( client, user_id )
  p "[db] line_account_has_been_locked"
  client.query("update social_setting SET line_status = 9 ,modified = now() WHERE user_id='#{user_id}'")
end

def reset_confirmation_code( client, user_id )
  p " [DB] reset_confirmation_code"
  client.query("update social_setting SET line_confirmation_code = '', line_login_ng_time=now() ,modified = now() WHERE user_id='#{user_id}'")
end
