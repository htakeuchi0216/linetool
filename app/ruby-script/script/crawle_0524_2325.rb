# coding: utf-8
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'selenium-webdriver'
require 'net/http'
require 'uri'
require 'json'
require 'open-uri'
require 'pp'
require 'mysql2'
require 'mail'
require_relative'db_library.rb'

$check_env = false
$save_all_cookie = false
$proxy_mail_test = false
$img_dir = "/var/www/html/cakephp/app/images/"

ipfile_path = File.expand_path('../../../Config/global_ip', __FILE__)
ip = File.read( ipfile_path, :encoding => Encoding::UTF_8)
ip.chomp!
abort("[ERROR]ip is not set") unless ip 

get_url  = "http://#{ip}/api/post_json.json"
post_url = "http://#{ip}/api/post_complete/"
certs = ["ajtja", "2015dmwmd03"]
$outpath = '/var/www/html/cakephp/app/webroot/files/capture'
$cookie_file_path = File.expand_path('../cookie/', __FILE__) 

$client = Mysql2::Client.new(
                            :host => "localhost",
                            :username => "root",
                            :password => "",
                            :database => "line")

res= open(get_url, {:http_basic_authentication => certs}).read
result = JSON.parse(res)

p "######################################################"
p "#### exec : #{Time.now.strftime('%Y-%m-%d %H:%M')} ###"
p "######################################################"

def disp_response_info(page)
  p "==== disp response_info ===="
  p "[status_code] : "
  p page.status_code
  p "[request_headers] :"
  pp page.driver.headers
  #p "[response_headers]"
  #pp page.response_headers
end

def save_cookie(page,user_id)
  p "[save_cookie]"
  #page.driver.cookies.each{|key, val|
  #  p "XXX === "+ val.name+"="+val.value+";"
  #}
  if $save_all_cookie == true
    cookie_str = ""
    page.driver.cookies.each{|key, val|
      #p " ** [cookie} "+ val.name+"="+val.value+";"
      cookie_str+= val.name+"="+val.value+";"
    }
    p "cookie_str : #{cookie_str}"
    # 必要な項目が設定されていれば、クッキー情報を保存する
    if cookie_str.include?("ponbro") && cookie_str.include?("ldsuid")
      cookie_outpath = $cookie_file_path+"/#{user_id}.txt"
      p "======== SAVE COOKIES TO #{cookie_outpath} ========="
      # cookieの内容のファイル書き出し
      File.write(cookie_outpath, cookie_str)  
    end
  else
    page.driver.cookies.each{|key, val|
      if key == "ponbro"
        cookie_outpath = $cookie_file_path+"/#{user_id}_ponbro.txt"
        p "======== SAVE COOKIES TO #{cookie_outpath} ========="
        File.write(cookie_outpath, val.value)
      end
    }
  end

end

def load_ponbro_cookie(user_id)
  p "LOAD COOKIE FILE"
    # COOKIEが存在すれば、セットする
    cookie_file_path_tmp = $cookie_file_path +"/#{user_id}_ponbro.txt"
    p "cookie file ? #{cookie_file_path_tmp}"
    cookie_str = ""
    if File.exist?(cookie_file_path_tmp )
       p "SET cookie FROM FILE"
      cookie_str  = File.read( cookie_file_path_tmp, :encoding => Encoding::UTF_8)
      p "return : [#{cookie_str}]"
      return cookie_str
    else
      p "NO cookie FILE"
      return
    end
end

def create_crawler
  $proxy = get_proxy_by_random($client)
  abort 'proxy error' if $proxy.empty?
  p "Use proxy:"
  p " - id #{$proxy['id']} ";
  p " - ip #{$proxy['ip']} ";
  Capybara.register_driver :poltergeist do |app|
    options = {
      :js_errors => false,
      :phantomjs_options => [
                             "--proxy=#{$proxy['ip']}:#{$proxy['port']}",
                             "--proxy-auth=#{$proxy['username']}:#{$proxy['password']}"
                            ]
    }

    obj = Capybara::Poltergeist::Driver.new(app, options)

    obj.headers = {
      #'User-Agent' => 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)',
      'User-Agent' => 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
      'accept-language' => ':en-US,en;q=0.8,ja;q=0.6',
      "Connection" => "keep-alive",
      "Cache-Control" => "max-age=0"
    }
    obj
  end

  Capybara.run_server        = false
  Capybara.current_driver    = :poltergeist
  Capybara.javascript_driver = :poltergeist
  Capybara.default_wait_time = 5
  Capybara.app_host = "https://admin-official.line.me/"
  if $check_env == true
    Capybara.app_host = "http://www.ugtop.com/spill.shtml"
    #Capybara.app_host = "http://tools.up2a.info/ja/requestheaders"
  end

  crawler = Crawler::Line.new
  crawler
end

module Crawler
  class Line
    include Capybara::DSL

    def pre_action(user_id)
      page.driver.clear_cookies
      if ponbro = load_ponbro_cookie(user_id)
        page.driver.set_cookie("ponbro",ponbro,{:domain => "admin-official.line.me"})
      else
        # REMOVE
        page.driver.remove_cookie("ponbro")
      end
    end

    def disp(msg)
      p "   #{msg}"
    end
    def save_image_and_html(suffix="")
      suffix = "_#{suffix}" if suffix != ""
      #p "[ERR] #{page.title} !='LINE@ MANAGER'"
      base = Time.now.strftime("%Y_%m%d_%H%M_%S")
      outputpath =  "#{$outpath}/#{base}#{suffix}.jpg"
      outputbodypath =  "#{$outpath}/body_#{base}#{suffix}.txt"

      disp "output :#{outputpath}"
      disp "output :#{outputbodypath}"
      File.write(outputbodypath, page.body)
      save_screenshot(outputpath);
    end

    def _login_confirmation(page)
      p "[A-600] --- login_confirmation ---"

      if page.has_text?("your account has been locked")
        line_account_has_been_locked($client,$user_id)
        save_image_and_html("account_has_been_locked")
        raise "account locked"
      end

      p "[A-610]"
      if !$confirmation_code.nil? && $confirmation_code != ""
        p "[A-620]"
        #確認番号の入力、ボタンクリック
        fill_in "code",
        :with => $confirmation_code
        click_button "Confirm"

        p " INPUT CONFIRM CODE(#{$confirmation_code})  AND Click Confirm BuTTON"
        reset_confirmation_code($client,$user_id)

        save_image_and_html("after_input_confirmation_code")
        return
      end
      p "[A-630]"
      if page.has_link?("Receive Confirmation Code")
        p " CLICk : 'Receive Confirmation Code'"
        click_link "Receive Confirmation Code"
        
        if page.has_button?("Send")
          #確認ポップアップのボタンを押す
          p "CLICK send Button"
          click_button "Send"
          confirmation_mail_is_sended($client,$user_id)
          save_image_and_html("after_send_confirmation_code")
        else
          save_image_and_html("there_is_no_send_link")
          p "[ERR]there is no Send link"
        end
      else
        p "there is no Cofirmation code link"
      end
      
    end

    def login_to_line(username,password,only_login_check=false)
      p "[ login to line ]"
      visit('')
      p ".. sleep 2 .."
      sleep(2)
      # IPアドレス確認
      if $check_env == true
        p page.body
        save_image_and_html("check_env")
        exit
      end

      # IPアドレスによる制限状態を検知
      if page.has_text?('Please enter the characters shown in the image')
        save_image_and_html("chapcha_img")
        p "THIS IP ADRESS IS limited . so, change proxy server";
        # proxyのステータスを更新する
        proxy_disable($client,$proxy["id"],$proxy["ip"])
        raise "this proxy is limited!"
      end

      save_image_and_html("before_inut_tid")
      p " input username,password and click LOGIN "
      fill_in "tid",
        :with => username
      fill_in "tpasswd",
        :with => password
      click_button "Login"

      save_image_and_html("after_click_login")
      if page.has_text?('Sorry. Either')
        p "login failure"
        line_login_failure($client,$user_id)
        return 0 if only_login_check == true
        raise "login failureXXX"
      elsif page.has_text?('Input your confirmation')
        p "111"
        _login_confirmation(page)
        p "[A-490]"
        if only_login_check == true
          p "[A-495]"
          p "VISIT TOP PAGE"
          visit('')
          p page.title
          if page.title =='LINE@ MANAGER'
            p "[A-499] confirmation code input success"
            p "[A-[4999] CALL SAVE COOKIE"
            save_cookie(page,$user_id)
            return 1
          end
          p "[A-500] not logged in"
          if !$confirmation_code.nil? && $confirmation_code != ""
            p "[A-510] bad confirmation code"+ $confirmation_code
            return 0
          else
            p "[A-511] no confirmation_code. send confirmation code"
            return 9
          end
        end
      else
        p "login Success??"
        p "page title :"
        p page.title
        if only_login_check == true 
          if page.title =='LINE@ MANAGER'
            return 1
          end
          return 0
        end
      end

    end

    def is_logged_in
      p "[ is_logged_in? ]" 
      visit('')
      #disp_response_info(page)

      p page.title
      if page.title !='LINE@ MANAGER'
        #disp_response_info(page)
        save_image_and_html("is_not_logged_in")
        p '[ERROR]is not loggedin' 
        return 0
      end
      line_login_success($client,$user_id)
      p "LOGIN SUCCESS "

      save_cookie(page,$user_id)

      return 1
    end

    def post(id,post_text,line_account_id)
      begin
        p "post : #{line_account_id}/home/send/"; 
        visit("#{line_account_id}/home/send/")
        p "after visit XX/home/send"
        #disp_response_info(page)
        
        save_image_and_html
        assert_text('New Post')
        click_button "Text"
        fill_in "body",:with => post_text

        # 画像添付 XXX

        #img_path = $img_dir + id + ".jpg"
        p "[debug]check img : #{img_path}"
        #if File.exist?(img_path)

        p "[debug]image exist!!"
        p "[debug]click photos"
        click_button "Photos"

        img_cnt = 0
        p "BEFORE UPLOAD IMAGEXXX"
        Dir.glob("/var/www/html/cakephp/app/images/#{id}_*").each{|img_path|
          p "#{img_cnt}- #{img_path}"
          within :xpath, "//div[@class='MdFRM03File image#{img_cnt}']" do
            p "[debug]attach file(#{img_path})"
            attach_file "file", img_path
            p ".. sleep 4 .."
            sleep(4)
            #wait nmae=media[0].objectId
          end
          img_cnt = img_cnt+1
        }
        save_image_and_html("after_attach_filesXXX")

        #click_button "Post"
        first(:button, "Post").click
        assert_text('Timeline(Home)')

        return 1 #success
      rescue => e
        puts $@
        p e.message
        raise e
        #return # failure
      end
    end

  end
end

if $proxy_mail_test == true
  p "====== proxy mail test =========="
  crawler = create_crawler
  proxy_disable($client,$proxy["id"],$proxy["ip"])
  exit
end

# 管理画面で、LINEアカウント情報が更新されたときに、
# 有効性確認のためにログインのみ実行する
if ARGV[0] == "only_login" 
  $user_id = ARGV[1]
  email = ARGV[2]
  pass  = ARGV[3]
  $confirmation_code = ARGV[4]
  p "ONLY LOGIN"
  p "user_id:#{$user_id}"
  p "email:#{email}"
  p "pass :#{pass}"
  if $confirmation_code
    p "confirmation_code :#{$confirmation_code}"
  end

  crawler = create_crawler
  status = crawler.login_to_line(email,pass,true)
  if status == 1
    p "LOGIN OK"
    exit 0
  elsif status == 9
    p "Confirm Mail Sended"
    exit 9
  end
  p "LOGIN NG"
  exit 1
end

if result['data'].empty?
  p " no post data" 
  exit
end

tmp = result['data'] || []
tmp.each{|user_id, value|
  p "==============="
  p "user_id :#{user_id}"
  $user_id = user_id

  begin
    username           = value['line_login_id']
    password           = value['line_login_password']
    line_status        = value['line_status']
    $confirmation_code = value['line_confirmation_code']#"457114"
    
    next if !username || !password    
    next if value["post"].empty?
    # 認証キーが必要な状態で、認証キーが取得できなかった場合スキップする
    p "=== line_status ; #{line_status} "
    if line_status == "3" 
      if  $confirmation_code.empty?
        p "user_id:#{$user_id} account needs input confirmation_code at setting page!!"
        next
      end
    end

    p " - username: [#{username}] , password : [XXXX] "
    p " - line_status : #{line_status}"
    p " - confirmation_code :[#{$confirmation_code}]"
    p " ----- post count : #{value['post'].count}"
    crawler = create_crawler


    crawler.pre_action(user_id)
    crawler.login_to_line(username,password)
    if crawler.is_logged_in == 0
      raise "IS NOT LOGGED IN[second]"
    end

    p "======== BEGIN POST ==========="
    value["post"].each{|hash|
      id              = hash["SocialPost"]["id"]
      body            = hash["SocialPost"]["body"]
      line_account_id = hash["SocialAccount"]["line_account_id"]
      p " = id :#{id}"
      p " = body :#{body}"
      p " = line_account_id : #{line_account_id}"
# 0802 comment out for debug
      if crawler.post(id,body,line_account_id)
        post_complete($client,id)
        increment_proxy_post_count($client,$proxy['id'])
      end
      #[重要]1回のバッチあたりの投稿数は1つのみとする
      break
    }
  rescue => e
    abort("check_env")if $check_env == true

    p "============ ERROR ================"
    puts $@
    p e.message
  end
}
