# coding: utf-8
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'selenium-webdriver'
require 'net/http'
require 'uri'
require 'json'
require 'open-uri'
require 'pp'
require 'mysql2'
require_relative'db_library.rb'

$check_env = false
$save_all_cookie = true

ipfile_path = File.expand_path('../../../Config/global_ip', __FILE__)
ip = File.read( ipfile_path, :encoding => Encoding::UTF_8)
ip.chomp!
abort("[ERROR]ip is not set") unless ip 

get_url  = "http://#{ip}/api/post_json.json"
post_url = "http://#{ip}/api/post_complete/"
certs = ["ajtja", "2015dmwmd03"]
$outpath = '/var/www/html/cakephp/app/webroot/files/capture'
$cookie_file_path = File.expand_path('../cookie/', __FILE__) 

$client = Mysql2::Client.new(
                            :host => "localhost",
                            :username => "root",
                            :password => "",
                            :database => "line")

res= open(get_url, {:http_basic_authentication => certs}).read
result = JSON.parse(res)
p "######################################################"
p "#### exec : #{Time.now.strftime('%Y-%m-%d %H:%M')} ###"
p "######################################################"
#confirmation_mail_is_sended($client,11)
#exit;

def disp_response_info(page)
  p "==== disp response_info ===="
  p "[status_code] : "
  p page.status_code
  p "[request_headers] :"
  pp page.driver.headers
  p "[response_headers]"
  pp page.response_headers
end

def save_cookie(page,user_id)
  p "[save_cookie]"
  cookie_str = ""

  if $save_all_cookie == true
    page.driver.cookies.each{|key, val|
      #p " ** [cookie} "+ val.name+"="+val.value+";"
      cookie_str+= val.name+"="+val.value+";"
    }
    p "cookie_str : #{cookie_str}"
    # 必要な項目が設定されていれば、クッキー情報を保存する
    if cookie_str.include?("ponbro") && cookie_str.include?("ldsuid")
      cookie_outpath = $cookie_file_path+"/#{user_id}.txt"
      p "======== SAVE COOKIES TO #{cookie_outpath} ========="
      # cookieの内容のファイル書き出し
      File.write(cookie_outpath, cookie_str)  
    end
  else
    page.driver.cookies.each{|key, val|
      if key == "ponbro"
        cookie_outpath = $cookie_file_path+"/#{user_id}.txt"
        p "======== SAVE COOKIES TO #{cookie_outpath} ========="
        File.write(cookie_outpath, val.name+"="+val.value+";")
      end
    }
  end

end

def load_cookie_file(user_id)
    # COOKIEが存在すれば、セットする
    cookie_file_path_tmp = $cookie_file_path +"/#{user_id}.txt"
    p "cookie file ? #{cookie_file_path_tmp}"
    cookie_str = ""
    if File.exist?(cookie_file_path_tmp )
       p "SET cookie FROM FILE"
      cookie_str  = File.read( cookie_file_path_tmp, :encoding => Encoding::UTF_8)
    else
      p "NO cookie FILE"
    end
  p "return : [#{cookie_str}]"
  cookie_str
end

def create_crawler
  $proxy = get_proxy_by_random($client)
  abort 'proxy error' if $proxy.empty?
  p "Use proxy:"
  p " - id #{$proxy['id']} ";
  p " - ip #{$proxy['ip']} ";
  cookie_str = load_cookie_file($user_id)

  Capybara.register_driver :poltergeist do |app|
    options = {
      :js_errors => false,
      :phantomjs_options => [
                             "--proxy=#{$proxy['ip']}:#{$proxy['port']}",
                             "--proxy-auth=#{$proxy['username']}:#{$proxy['password']}"
                            ]
    }

    obj = Capybara::Poltergeist::Driver.new(app, options)
    tmp = {
      #'User-Agent' => 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)',
      'User-Agent' => 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
      'accept-language' => ':en-US,en;q=0.8,ja;q=0.6',
      #'Cookie'  => load_cookie_file($user_id),
      "Connection" => "keep-alive",
      "Cache-Control" => "max-age=0"
    }
    tmp["Cookie"] = cookie_str if !cookie_str.nil? && cookie_str != ''
    obj.headers = tmp
=begin
Host: admin-official.line.me
User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.7,ja;q=0.3
Accept-Encoding: gzip, deflate
Cookie: ponbro=bhaBao4OIZCWUe8p3anZMadKGVnQgi7N; ldsuid=CoIqS1VeEv92VQmPBp3WAg==; minesota=ZR5kLZbS8NHCBee7poNec7yOGEFVkNIaIqDdW0DSPIHQZYdu958j6SkSiCyAb_UGRk-IPmX7b33y3AX4wC4GQsPnEihmrvsmhwy-nE1Zd7o; _ga=GA1.2.1803463932.1432228642; _gat=1
Connection: keep-alive
Cache-Control: max-age=0
=end

    obj
  end

  Capybara.run_server        = false
  Capybara.current_driver    = :poltergeist
  Capybara.javascript_driver = :poltergeist
  Capybara.default_wait_time = 5
  #Capybara.app_host = "https://admin-official.line.me/1146404/"
  Capybara.app_host = "https://admin-official.line.me/"
  if $check_env == true
    Capybara.app_host = "http://sns-autopost.com/api/cookie_test.json"
  end

  crawler = Crawler::Line.new
  return crawler,cookie_str
end

module Crawler
  class Line
    include Capybara::DSL

    def disp(msg)
      p "   #{msg}"
    end
    def save_image_and_html(suffix="")
      suffix = "_#{suffix}" if suffix != ""
      #p "[ERR] #{page.title} !='LINE@ MANAGER'"
      base = Time.now.strftime("%Y_%m%d_%H%M_%S")
      outputpath =  "#{$outpath}/#{base}#{suffix}.jpg"
      outputbodypath =  "#{$outpath}/body_#{base}#{suffix}.txt"

      disp "output :#{outputpath}"
      disp "output :#{outputbodypath}"
      File.write(outputbodypath, page.body)
      save_screenshot(outputpath, :full => true );
    end

    def _login_confirmation(page)
      p " --- login_confirmation ---"

      if page.has_text?("your account has been locked")
        line_account_has_been_locked($client,$user_id)
        save_image_and_html("account_has_been_locked")
        raise "account locked"
      end

      if $confirmation_code != ""
        #確認番号の入力、ボタンクリック
        fill_in "code",
        :with => $confirmation_code
        click_button "Confirm"

        p " INPUT CONFIRM CODE(#{$confirmation_code})  AND Click Confirm BuTTON"
        reset_confirmation_code($client,$user_id)

        save_image_and_html("after_input_confirmation_code")
        return
      end
      if page.has_link?("Receive Confirmation Code")
        p " CLICk : 'Receive Confirmation Code'"
        click_link "Receive Confirmation Code"
        
        if page.has_button?("Send")
          #確認ポップアップのボタンを押す
          p "CLICK send Button"
          click_button "Send"
          confirmation_mail_is_sended($client,$user_id)
          save_image_and_html("after_send_confirmation_code")
        else
          save_image_and_html("there_is_no_send_link")
          p "[ERR]there is no Send link"
        end
      else
        p "there is no Cofirmation code link"
      end
      
    end

    def login_to_line(username,password)
      p "[ login to line ]"
      visit('')



      # IPアドレス確認
      if $check_env == true
        p page.body

        #save_image_and_html("check_env")
        exit
      end

      # IPアドレスによる制限状態を検知
      if page.has_text?('Please enter the characters shown in the image')
        save_image_and_html("chapcha_img")
        p "THIS IP ADRESS IS limited . so, change proxy server";
        # proxyのステータスを更新する
        proxy_disable($client,$proxy["id"],$proxy["ip"])
        raise "this proxy is limited!"
      end

      #p "title:" + page.title
      p " input username,password and click LOGIN "
      fill_in "tid",
        :with => username
      fill_in "tpasswd",
        :with => password
      click_button "Login"

      save_image_and_html("after_click_login")
      if page.has_text?('Sorry.')
        p "login failure"
        line_login_failure($client,$user_id)
        raise "login failureXXX"
      elsif page.has_text?('Input your confirmation')
        _login_confirmation(page)
      else
        p "login Success??"
      end

    end

    def is_logged_in
page.driver.remove_cookie("ponbro")
page.driver.set_cookie("ponbro", "boJviEfFZHGd9x7nphrb1ed6VN7eXqUG")
      p "[ is_logged_in? ]" 
      visit('')
      disp_response_info(page)

      save_image_and_html("xxxxxxxxx1__")

      p page.title
      if page.title !='LINE@ MANAGER'
        disp_response_info(page)
        save_image_and_html("is_not_logged_in")
        p '[ERROR]is not loggedin' 
        return 0
      end
      line_login_success($client,$user_id)
      p "LOGIN SUCCESS "
      #visit("user/account")
      #save_image_and_html("xxxxxxxxx2222__")
      #p page.title
      #assert_text('Basic Info')
      #p "EXIT"
      #exit

      save_cookie(page,$user_id)
      return 1
    end

    def post(post_text,line_account_id)
      begin
        p "post : #{line_account_id}/home/send/"; 
        visit("#{line_account_id}/home/send/")
        p " ================ POST ================"
        disp_response_info(page)
        p " ================ POST end================"
        
        save_image_and_html("visit_to_send")
        assert_text('New Post')
        click_button "Text"
        fill_in "body",
        :with => post_text
        #click_button "Post"

        #first(:button, "Post").click
        #find('.mdBtn02Txt')[0].click()
        page.all('.mdBtn02Txt')[1].click

        save_image_and_html("DISP_TIMELINE_before")
        assert_text('Data Statistics')
        save_image_and_html("DISP_TIMELINE")

        return 1 #success
      rescue => e
        puts $@
        p e.message
        raise e
        #return # failure
      end
    end


  end
end

if $check_env == true
  #crawler = create_crawler
  crawler, cookie_str = create_crawler
  crawler.login_to_line("aaa","bbb")
  exit
end

if result['data'].empty?
  p " no post data" 
  exit
end

tmp = result['data'] || []
#########
=begin
if ARGV[0] == "re_send_confirmation_code"
  p " RE SEND CONFIRMATION CODE "
  id = ARGV[1]
  value = tmp[id]
  if id.nil?
    p "[ERR]no id "
  elsif value.nil? || value.empty?
    p "[ERR]can't get user data (#{id})"
  else 
    username           = value['line_login_id']
    password           = value['line_login_password']
    line_status        = value['line_status']
    $confirmation_code = ""#XXX
#    pp value
    crawler = create_crawler
    crawler.login_to_line(username,password)

    crawler.is_logged_in

  end
  exit
end
=end

tmp.each{|user_id, value|
  p "==============="
  p "user_id :#{user_id}"
  $user_id = user_id

  begin
    username           = value['line_login_id']
    password           = value['line_login_password']
    line_status        = value['line_status']
    $confirmation_code = value['line_confirmation_code']#"457114"
    
    p "TEST   ==============="
    #crawler = create_crawler(user_id)
    #pp crawler.methods
    #crawler = create_crawler
    #crawler = load_object(user_id)

    next if !username || !password    
    next if value["post"].empty?
    # 認証キーが必要な状態で、認証キーが取得できなかった場合スキップする
    p "=== line_status ; #{line_status} "
    if line_status == "3" 
      if  $confirmation_code.empty?
        p "user_id:#{$user_id} account needs input confirmation_code at setting page!!"
        # next
      end
    end

    p " - username: [#{username}] , password : [#{password}] "
    p " - line_status : #{line_status}"
    p " - confirmation_code :[#{$confirmation_code}]"
    p " ----- post count : #{value['post'].count}"
    #crawler = create_crawler
    crawler, cookie_str = create_crawler
    p " XXX cookie_str; #{cookie_str}"

    if cookie_str.nil? || cookie_str == ''
      crawler.login_to_line(username,password)
      if crawler.is_logged_in == 0
        raise "IS NOT LOGGED IN[second]"
      end
    end

    if crawler.post("05250007--","1146404")
      #post_complete($client,id)
      #increment_proxy_post_count($client,$proxy['id'])
    end

    exit
    p "======== BEGIN POST ==========="
    value["post"].each{|hash|
      id              = hash["SocialPost"]["id"]
      body            = hash["SocialPost"]["body"]
      line_account_id = hash["SocialAccount"]["line_account_id"]
      p " = id :#{id}"
      p " = body :#{body}"
      p " = line_account_id : #{line_account_id}"
      p " body : #{body}"
      if crawler.post(body,line_account_id)
        post_complete($client,id)
        increment_proxy_post_count($client,$proxy['id'])
      end
    }
  rescue => e
    abort("check_env")if $check_env == true

    p "============ ERROR ================"
    puts $@
    p e.message
  end
}
