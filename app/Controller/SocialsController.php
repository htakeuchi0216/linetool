<?php
App::uses('AppController', 'Controller');
App::uses('FB', 'Facebook.Lib');
//require 'facebook-php-sdk/src/facebook.php';
App::import('vendor', 'facebook/php-sdk/src/facebook');
App::import('Vendor', 'facebook/src/facebook');

App::uses('AppShell','Console/Command');
App::uses('GetSocialPostsShell','Console/Command');

class SocialsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
  public $uses = array("SocialAccount","SocialPost","SocialSetting");
  public $components = array('SocialCommon');

  //opauth complete
  public function opauth_complete() {
    $id = $this->Session->read('_Opauth.social_account_id');
    $this->Session->delete('_Opauth.social_account_id');
    //print "social_account_id : $id のFacebookアカウントの設定を行いますXXX";
    $data = array(
      'id'                    => $id,
      'user_id'               => $this->Auth->user('id'),
      #'facebook_account_id'   => $this->data['auth']['uid'],
      #'facebook_display_name' => $this->data['auth']['info']['name'],
      'facebook_token'        => $this->data['auth']['credentials']['token'],
      'facebook_token_expires'=> $this->data['auth']['credentials']['expires'],
    );
    //debug($data);
    if($this->SocialAccount->save($data)){
      $this->Session->setFlash('Facebookアカウント連携成功');
    }else{
      $this->Session->setFlash('Facebookアカウント連携失敗');
    }
    return $this->redirect(array('action' => 'index'));
  }
  public function fb_reset() {
    $id =  $this->request->params['id'];
    $social_account =$this->SocialAccount->find(
      'all',
      array(
	'conditions' => array(
	  'SocialAccount.id'      => $id,
	  'SocialAccount.user_id' => $this->Auth->user('id'),
	)
      )
    );
    if($social_account){
      $data = array(
	'id'                    => $id,
	'facebook_account_id'   => '',
	'facebook_display_name' => '',
	'facebook_token'        => '',
	'facebook_token_expires'=> '',
      );
      if($this->SocialAccount->save($data)){
	$this->Session->setFlash('reset完了');
      }
    }else{
      $this->Session->setFlash('no data reset 失敗');
    }
    return $this->redirect(array('action' => 'index'));
  }

  public function index() {
    if ($this->request->is('post')) {
      foreach ( $this->request->data['SocialAccounts'] as $account){
        if(!isset($account['id']) ){
          $this->SocialAccount->create();
        }

        $result = $this->SocialAccount->save( $account);
        if( is_array($result)  ){
          $this->Session->setFlash('保存しました');
        }else{
          if (is_array($this->SocialAccount->validationErrors ) ){
            $msg = "";
            foreach( $this->SocialAccount->validationErrors as $e ){
              $msg[] = $e[0];
            }
            $this->Session->setFlash( join("<br>" , $msg ) );
          }else{
            $this->Session->setFlash('保存に失敗しました');
          }
          #$this->Session->setFlash( print_r( $this->SocialAccount->validationErrors, true) );
        }
      }
      //$this->Session->setFlash('入力完了');
      return $this->redirect(array('action' => 'index'));
    }
    $user_id = $this->Auth->user('id');
    $social_accounts =$this->SocialAccount->find(
      'all',
      array(
            'conditions' => array(
                                  'SocialAccount.user_id' => $user_id
                                  ),
            'order' => 'SocialAccount.id'
            )
                                                 );
    $social_setting_status = $this->SocialSetting->get_status($user_id);
    $this->set('status', $social_setting_status );
    $this->set('accounts', $social_accounts );
  }

  /*
  private function _get_facebook_post($social_account_id,$account_id,$token,$since_time=null){
    if(!$token){//tokenが取得できない場合空を返却
      return array();
    }
    $this->facebook = new Facebook( array(
      'appId' => '1424914961142418',
      'secret' => 'f89455c6ff04fb698e2e65f0296959ff',//XXX
      'cookie' => true
    ));
    
    $option = array('access_token' => $token);
    if($since_time){
      //$option['since'] = strtotime($since_time);
    }

    $feeds = $this->facebook->api("/me/posts",
				  'GET',
				  $option
    );
    $result = array();
    if( isset( $feeds['data'] )){
      foreach ( $feeds['data'] as $feed) {
	$tmp = $this->make_facebook_posts_data($feed);
	$tmp['SocialPost']['social_account_id'] = $social_account_id;
	$tmp['SocialPost']['account_id']       =  $account_id;
	$result[] = $tmp;
      }
    }
    return $result;
  }
  */

  private function _save_tweet($social_account_id,$tw_response){
    foreach ($tw_response as $tweet){
      $created_at = strtotime( $tweet['created_at'] );
      $data = array(
	'SocialPost' =>array(
	  'social_account_id' => $social_account_id,
	  'account_id' => $tweet['user']['screen_name'],
	  'type'       => 1 , #1:tw , 2:fb
	  'post_id'    => $tweet['id_str'],
	  'body'       => $tweet['text'],
	  'original_posted' => date('Y-m-d H:i:s',$created_at),
	  'is_posted'  => 0
	)
      );
      print "twitterデータ取り込みXXX<BR/>";//XXX
      $this->SocialPost->save($data);
      $this->SocialPost->create();
    }
  }

  private function _get_max_date($social_account_id,$screen_name,$max_id){
    $result = $this->SocialPost->find(
      'first',
      array(
	'conditions' => array(
	  'SocialPost.post_id' => $max_id,
	)
      )
    );
    if( $result ){
      return $result['SocialPost']['original_posted'];
    }
  }

  private function _get_max_id($social_account_id,$screen_name,$type=1){
    $result = $this->SocialPost->find(
      'first',
      array(
	"fields" => "MAX(SocialPost.post_id) as max_id",
	'conditions' => array(
	  'SocialPost.type' => $type,
	  'SocialPost.social_account_id' => $social_account_id,
	  'SocialPost.account_id'        => $screen_name
	),
      )
    );
    $max_id = null;
    if(isset($result[0]['max_id'])){
      $max_id = $result[0]['max_id'];
    }
    return $max_id;
  }

  public function post_list() {
    $user_id = $this->Auth->user('id');
    $social_accounts = $this->SocialAccount->get_by_user_id($user_id);
    $debug = 1;
    $data = array();
    foreach($social_accounts as $account){
      $account_id = $account['SocialAccount']['id'];
      $posts = $this->SocialPost->find(
	'all',
	array(
	  'conditions' => array(
	    'SocialPost.social_account_id' => $account_id,
	  ),
	  'order' => 'SocialPost.original_posted DESC'
	)
      );
      $data[$account_id] = $posts;

    }
    $this->set('lists', $data );
  }

  public function get_posts() {
    if ( !exec('sh /var/www/html/cakephp/app/get_social_post.sh 2>&1',$array)) {
      //command失敗を検知して処理したい
      echo "NG";
    }
    //var_dump($array);
    print join("<BR/>",$array);
    #nl2br($array.join());
    /*
    $user_id = $this->Auth->user('id');
    $social_accounts = $this->SocialAccount->get_by_user_id($user_id);
    $debug = 1;
    foreach($social_accounts as $account){
      $data = $account['SocialAccount'];
      $fb = $data['facebook_account_id'];//XXX
      $tw = $data['twitter_account_id'];//XXX
      
      $flg = 1;
      if($debug){
	if( $tw  && $fb){
	  print "Twitter:[$tw] , Facebook:[$fb]から投稿データを取得します";
	}elseif($tw){
	  print "Twitter:[$tw] から投稿データを取得します";
	}elseif($fb){
	  print "Facebook:[$fb] から投稿データを取得します";
	}else{
	  $flg = 0;
	  print "取得対象のアカウント設定がされていません。";
	}
      }
      print "AAA";
      // FB投稿データの取得、保存

      $social_account_id = $data['id'];
      $account_id        = $data['facebook_account_id'];

      // Twitter投稿データの取得、保存
      $screen_name       = $data['twitter_account_id'];
      $social_account_id = $data['id'];
      $max_id = $this->SocialPost->get_twitter_max_post_id($social_account_id,$screen_name);
      $tw_posts = $this->SocialCommon->get_twitter_post($screen_name,$max_id,$user_id);
      $this->_save_tweet( $social_account_id , $tw_posts );

      #print "fbデータ取り込み";
      //$max_id     = $this->_get_max_id($social_account_id,$account_id,2);
      $max_id = $this->SocialPost->get_facebook_max_post_id($social_account_id,$account_id);
      $since_time = $this->_get_max_date($social_account_id,$account_id,$max_id);
      $token = $data['facebook_token'];
      $fb_posts = $this->SocialCommon->get_facebook_post($social_account_id,$account_id,$token,$since_time);

      foreach( $fb_posts as $post){
	$this->SocialPost->create(false);
	$this->SocialPost->save($post);
      }
    }
    */
    exit();
  }

  private function make_facebook_posts_data($feed){
    $ids = preg_split('/_/', $feed['id'] );
    $fb_user_id = $ids[0];
    $post_id    = $ids[1];
    $created_time = $feed['created_time'];
    //$body = "";
    $body = $this->make_facebook_body($feed);
    
    $data = array(
      'SocialPost' =>array(
	//'social_account_id' => $account_id,
	//'account_id' => $account_id,
	'type'       => 2 , #1:tw , 2:fb
	'post_id'    => $post_id,
	'body'       => $body,
	'original_posted' => $created_time,
	'is_posted'  => 0
      )
    );
    return $data;
  }
  private function make_facebook_body($feed){
    $body_parts = array();
    if(isset($feed['name']) ){
      $body_parts[] = $feed['name'];
    }
    if(isset($feed['link']) ){
      $body_parts[] = $feed['link'];
    }
    
    if(isset($feed['message']) ){
      $body_parts[] = $feed['message'];
    }
    //if(isset($feed['description']) ){
    //$body= $feed['description'];
    //}
    
    $body = join("\n",$body_parts);
    return $body;
  }

  public function change_available(){
    if(!isset($this->request->params['id']) ){
      return $this->redirect(array('action' => 'index'));
    }

    $id =  $this->request->params['id'];
    $data =$this->SocialAccount->find(
      'first',
      array(
	'conditions' => array(
	  'SocialAccount.user_id' => $this->Auth->user('id'),
	  'SocialAccount.id' => $id,
	)
      )
    );
    if(!$data){
      print "exit";
      exit();
    }
    $params = array( 'id' => $id);
    switch ( $this->request->params['type'] ) {
    case "tw":
      if( $data['SocialAccount']['is_twitter_available'] ){
	$params['is_twitter_available'] = 0;
      }else{
	$params['is_twitter_available'] = 1;
      }
      break;
    case "fb":
      if( $data['SocialAccount']['is_facebook_available'] ){
	$params['is_facebook_available'] = 0;
      }else{
	$params['is_facebook_available'] = 1;
      }
      break;
    }
    $this->SocialAccount->save($params);
    return $this->redirect(array('action' => 'index'));
  }

  public function post_sample(){
    $social_accounts = $this->SocialAccount->get_by_user_id($this->Auth->user('id') );
    print "<table border=1>";
    print "<tr><td>id</td><td>twitter</td><td>Tw投稿基準日</td>";
    print "<td>facebook</td><td>FB投稿基準日</td><td>line</td>";
    foreach( $social_accounts as $account){
      $id = $account['SocialAccount']['id'];
      $tw = $account['SocialAccount']['twitter_account_id'];
      $fb = $account['SocialAccount']['facebook_account_id'];
      $ln = $account['SocialAccount']['line_account_id'];

      $tw_time = $account['SocialAccount']['twitter_available_datetime'];
      $fb_time = $account['SocialAccount']['facebook_available_datetime'];
      $ln_time = $account['SocialAccount']['line_available_datetime'];

      print "<tr><td>$id</td><td>$tw</td><td>$tw_time</td>";
      print "<td>$fb</td><td>$fb_time</td>";
      print "<td>$ln</td></tr>";
      //      print $account['SocialAccount']
    }
    print "</table>";
    print "※上記[投稿基準日]以降の投稿データをLINE@へ投稿を行いますす";

    foreach( $social_accounts as $account){
      $id = $account['SocialAccount']['id'];
      $tw_time = $account['SocialAccount']['twitter_available_datetime'];
      $fb_time = $account['SocialAccount']['facebook_available_datetime'];
      $ln_time = $account['SocialAccount']['line_available_datetime'];

      $posts = $this->SocialPost->find(
	'all',
	array(//XXX
	  'conditions' => array(
	    'SocialPost.social_account_id' => $id,
	      //'SocialPost.type' => 1,
	      //'SocialPost.original_posted' => '>' . $tw_time//XXX
	  ),
	  'order' => 'SocialPost.original_posted DESC'
	)
      );
      print "<h2>ID : {$id}</h2>";
      print "<table border=1>";
      print "<tr><td>id</td><td>post_id</td><td>body</td><td>type</td>";
      print "<td>投稿済みフラグ</td><td>オリジナル投稿日時</td><td>作成日時</td></tr>";
      foreach( $posts as $tmp){

	$post = $tmp['SocialPost'];
	print "<tr>";
	print "<td>{$post['id']}</td>";
	print "<td>{$post['post_id']}</td>";
	print "<td>{$post['body']}</td>";
	if($post['type']==1){
	  print "<td>twitter</td>";
	}else{
	  print "<td>facebook</td>";
	}
	print "<td>{$post['is_posted']}</td>";
	print "<td>{$post['original_posted']}</td>";
	print "<td>{$post['created']}</td>";
	print "</tr>";
      }
      print "</table>";

    }
    print "POST SAMPLE";
    exit();
  }

}
