<?php

class ApiController extends AppController {
  var $name = "Api";
  public $uses = array( 'User',"SocialAccount","SocialPost");
  public $components = array('SocialCommon');

  public function beforeFilter()
  {
    // ipアドレス制限
    // 同一サーバーでrubyスクリプトを動かしているのであれば、同一サーバーのIPを許可する
    // IPアドレス確認コマンド
    // curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'
    #print $_SERVER['REMOTE_ADDR'];    
    $allow_ips = array("219.1.158.102","126.196.101.146", Configure::read('GlobalIP') );
    if (!in_array($_SERVER['REMOTE_ADDR'], $allow_ips)) {
      echo "Access Error";
      exit();
    }
    parent::beforeFilter();
    $this->Auth->allow('post_json','post_json2','post_complete');
  }

  private function decrypt($value){
    if($value){
      return Security::rijndael(base64_decode($value), Configure::read('Security.db.key'), 'decrypt');
    }
    return "";
  }
  public function post_complete($id){
    $id =  $this->request->params['id'];

    $result = $this->SocialPost->find( 'first', array('conditions' => array( 'SocialPost.id' => $id)  )  );
    if( $result){
      $data = array(
                    'SocialPost' =>array(
                                         'id' => $id,
                                         'is_posted' => 1,
                                         'line_posted' => date('Y-m-d H:i:s')
                                         )
      );

      $this->viewClass = 'Json';
      $this->response->statusCode(200);
      $this->set('msg', "complete");
      $this->set('_serialize', array('msg'));
    }
  }

  public function post_json(){
    $this->viewClass = 'Json';
    $result = $this->_get_data();
    $this->set('data',$result);
    $this->set('_serialize', array('data'));
  }

  public function post_json2(){
    $this->viewClass = 'Json';
    $result = $this->_get_data();
    print "<pre>";
    print_r($result);
    print "</pre>";
    exit();
  }

  public function _get_data(){
    $sql = "
 SELECT u.id, 
     sa.id,
    line_account_id as line_account_id ,
    CASE  WHEN is_twitter_available = TRUE  THEN sa.twitter_account_id ELSE '' END   as twitter_account_id,
    CASE  WHEN is_facebook_available = TRUE  THEN sa.facebook_account_id ELSE '' END as facebook_account_id,
    sa.facebook_token as facebook_token,
    is_line_available,
    is_twitter_available,
    is_facebook_available,
    line_available_datetime,
    twitter_available_datetime,
   facebook_available_datetime,
    ss.*
 FROM users u
LEFT JOIN social_accounts sa ON u.id = sa.user_id
LEFT JOIN social_setting  ss ON ss.user_id = sa.user_id
WHERE u.is_deleted = 0 and sa.is_deleted = 0 and
CASE WHEN ss.line_status = 2  THEN ss.modified > ss.line_login_ng_time ELSE 1 = 1 end
";
    # line_status = 2 (ログイン失敗)の場合、情報が更新された場合のみ対象とする

    $records = $this->SocialAccount->query($sql);
    $result = array();
    foreach($records as $data ){
      $user_id           = $data['u']['id'];
      // 投稿格納配列初期化
      if ( !isset($result[$user_id]['post'])) {
        $result[$user_id]['post'] = array();
      }

      $social_account_id = $data['sa']['id'];
      $result[$user_id]['user_id']                = $user_id;
      $result[$user_id]['line_login_id']          = $data['ss']['line_login_id'];
      $result[$user_id]['line_login_password']    = $this->decrypt($data['ss']['line_login_password']);
      $result[$user_id]['line_status']            = $data['ss']['line_status'];
      $result[$user_id]['line_confirmation_code'] = $data['ss']['line_confirmation_code'];
      //debug($data);
      $datetime = null;
      if( $data['sa']['line_account_id'] ){
      #print "LINE ACCOUNT OK\n";
        $account_id = null;

        // Twitter取得
        if($data[0]['twitter_account_id'] ){
          $account_id = $data[0]['twitter_account_id'];
          $datetime = $data['sa']['twitter_available_datetime'];
          $tw_target_posts = $this->SocialPost->get_post_data($social_account_id,$account_id,1,$datetime);
          #print "<pre>";
          #print_r($tw_target_posts);
          #print "</pre>";
          $result[$user_id]['post'] = array_merge($tw_target_posts, $result[$user_id]['post'] );
        }
        // Facebook取得
        if($data[0]['facebook_account_id'] ){
          $account_id = $data[0]['facebook_account_id'] ;
          $datetime = $data['sa']['facebook_available_datetime'];
          $fb_target_posts =  $this->SocialPost->get_post_data($social_account_id,$account_id,2,$datetime);
          $result[$user_id]['post'] = array_merge($fb_target_posts, $result[$user_id]['post'] );
        }
      }
      #投稿対象がなければ連想配列から削除
      if( empty($result[$user_id]['post']) ){
        unset( $result[$user_id]);
      }
    }
    if($debug = 0 ){
      print "<pre>";
      print_r($result);
      print "</pre>";
      exit();
    }
    return $result;
  }

}