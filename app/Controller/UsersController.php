<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
class UsersController extends AppController {
  //読み込むコンポーネントの指定
  public $components = array('Session', 'Auth', 'Paginator');
  public $uses = array( 'User' , 'SocialSetting');

  //どのアクションが呼ばれてもはじめに実行される関数
  public function beforeFilter()
  {
    parent::beforeFilter();
    
    //未ログインでアクセスできるアクションを指定
    //これ以外のアクションへのアクセスはloginにリダイレクトされる規約になっている
    $this->Auth->allow('register', 'login', 'opauth_complete');
  }
  
  //ログイン後にリダイレクトされるアクション
  public function index(){
    $this->set('user', $this->Auth->user());
    $this->set('users', $this->Paginator->paginate());
  }

  public function update_password(){
    if($this->request->is('post') || $this->request->is('put')){
      if($this->User->save($this->request->data)){
        $this->Session->setFlash('パスワードを変更しました');
      }
      else{
        $this->Session->setFlash('パスワードの変更に失敗しました');
      }
    }
    $this->redirect('edit');
  }

  public function edit(){
    if($this->request->is('post') || $this->request->is('put')){
      if($this->User->save($this->request->data)){
        $this->Session->setFlash('変更しました');
      }
      else{
        $this->Session->setFlash('保存失敗しました');
      }
      $this->redirect('edit');
    }
    $user_id = $this->Auth->user('id');
    $user = $this->User->find(
      'first',
      array('conditions' => array("User.id" => $user_id  ) )
    );
    $this->set('user', $user['User']  );
    $this->request->data = $user;
  }

  public function setting(){
    $user_id = $this->Auth->user('id');

    if($this->request->is('post') || $this->request->is('put')){

      if (!isset($this->requet->data['SocialSetting']['id']) ){
        $this->SocialSetting->create();
      }

      //空白の削除
      $keys=array('twitter_consumer_key','twitter_consumer_secret',  'twitter_access_token','twitter_access_token_secret',  'line_login_id','line_login_password');
      foreach ($keys as $key ){
        $this->request->data['SocialSetting'][$key] = trim($this->request->data['SocialSetting'][$key]);
      }
      $result = $this->SocialSetting->save($this->request->data);
      if( is_array($result) ){
        $this->Session->setFlash('保存しました');
      }
      else{
        $this->log("@@@@@ Setting save error " ,LOG_DEBUG);
        $this->log( print_r( $this->SocialSetting->validationErrors ,true ) ,LOG_DEBUG);
        $this->Session->setFlash('保存失敗しました');
        $msg = array("");
        foreach( $this->SocialSetting->validationErrors as $e ){
          $msg[] = $e[0];
        }
        $msg = array_filter($msg);#空白要素を削除
        $this->Session->setFlash( join("<br>" , $msg ) );
        #$this->Session->setFlash('サクセスメッセージ', 'default', array('class' => 'flash_success'));
      }
      //$this->set( 'valerror', $this->SocialSetting->validationErrors);
      //return $this->redirect('setting');
    }
    $social_setting = $this->SocialSetting->find_by_user_id($user_id);
    $this->set('user_id', $user_id );
    $this->set('social_setting', $social_setting );
    $this->request->data = $social_setting;
  }

  public function register(){
    //$this->basic_auth();
    //////////////
    //$this->requestにPOSTされたデータが入っている
    //POSTメソッドかつユーザ追加が成功したら
    if($this->request->is('post') && $this->User->save($this->request->data)){
      //ログイン
      //$this->request->dataの値を使用してログインする規約になっている
      $this->Auth->login();
      return $this->redirect('index');
    }
  }

  public function login(){
    $this->Auth->authenticate = array('Form' => Array('scope' => array('is_deleted' => '0')));
    if($this->request->is('post')) {
      if($this->Auth->login())
        return $this->redirect('index');
      else
        $this->Session->setFlash('ログイン失敗');
    }
  }
  
  public function logout(){
    $this->Auth->logout();
    $this->redirect('login');
  }

}

