<?php
App::uses('TwitterOAuth', 'Vendor/twitteroauth/');
App::import('vendor', 'facebook/php-sdk/src/facebook');
App::import('Vendor', 'facebook/src/facebook');

class SocialCommonComponent extends Component {
  var $components = array('Session','Rss');

  public function initialize(Controller $controller) {
    $this->controller = $controller;
  }

  function get_facebook_account_id($name){
    $json = @file_get_contents("https://graph.facebook.com/{$name}");
    if($json){
      $tmp = json_decode($json);
      if(!$tmp->id){
        die("can't get facebook account id XX");
      }
      return $tmp->id;
    }
  }

  function get_facebook_post_from_rss($social_account_id,$account_id,$token,$since_time=null){
    $facebook_id = $this->get_facebook_account_id($account_id);
    $url = "http://www.facebook.com/feeds/page.php?id={$facebook_id}&format=rss20";
    $this->log($url,LOG_DEBUG);
    try {
      $rss = $this->Rss->read( $url );
    } catch( InternalErrorException $e) {
      $rss = array();
    }
    $result = array();
    foreach ($rss as $r){
      if (preg_match('/syndication_error.php/', $r->link)){
        $this->log(" $account_id($facebook_id) : syndication_error  " );
        continue;
      }
      $post_id = null;
      if (preg_match('/https?:\/\/www.facebook.com\/[\w\.]+\/posts\/(\d*)/', $r->link, $match)){
        $post_id = $match[1];
      }elseif (preg_match('/https?:\/\/www.facebook.com\/[\w\.]+\/photos\/[\w\.]+\/(\d*)/', $r->link, $match)){ 
        $post_id = $match[1];
      }else{ #post_idが取得できない場合は保存を行わない
        $this->log(" unkwown Facebook URL : {$r->link} " ,LOG_DEBUG );

        continue;
      }
      $tmp = array(
                   'SocialPost' =>array(
                                        'social_account_id' => $social_account_id,
                                        'account_id' => $account_id,
                                        'type'       => 2 , #1:tw , 2:fb
                                        'post_id'    => $post_id,
                                        'title'      => (string)$r->title,
                                        'body'       => (string)$r->description,
                                        'original_posted' => (string)$r->pubDate,
                                        'is_posted'  => 0
                                        )
                   );
      $result[] = $tmp;
    }
    return $result;
  }
  
  function get_facebook_post_from_api($social_account_id,$account_id,$token,$since_time=null){
    #$account_id = "kodamaayumu";#XXXX
    $url = "https://graph.facebook.com/v2.3/{$account_id}/posts?access_token={$token}";
    $this->log($url,LOG_DEBUG);
    $json = @file_get_contents($url);
    if($json){
      $this->log("success api call",LOG_DEBUG);
      $tmp = json_decode($json,true);
      $result = array();
      foreach($tmp['data'] as $data ){
        $post_id = "";
        if (preg_match('/(\d*)_(\d*)/', @$data['id'], $match)){
          $this->log( "MATCH!!!!"  ,LOG_DEBUG );
          $this->log( print_r($match ,true)  ,LOG_DEBUG );
          $post_id = $match[2];
        }
        if(!$post_id){
          $this->log( "can't get post_id. do skip"  ,LOG_DEBUG );
          continue;
        }
        
        $title   = @$data['name'];
        $message = @$data['message'];
        $created = @$data['created_time'];#$create
        #$this->log( print_r($data ,true)  ,LOG_DEBUG );
        # TODO 0729 画像保存
        # "https://graph.facebook.com/v2.3/{$account_id}/posts?access_token={$token}";
        $this->log( "XXXXXXXXX APICHECK-FACEOOK XXXXXXXXXXX" ,LOG_DEBUG );
        $this->log( "@@@@@@FACEBOOK API@@@@@@@@@@@@" ,LOG_DEBUG );
        #$this->log( print_r($data ,true)  ,LOG_DEBUG );
        $this->log( "XXXOBJECT_ID : ". $data['object_id']  ,LOG_DEBUG );
        $img_url = "";
        if($picture = @$data['picture'] ){
          $this->log( "XXXXXXXXXXHAVE PICTUREXXXXXXXXXXXXXX" ,LOG_DEBUG );
          #$this->log( $picture ,LOG_DEBUG );
          #$this->log( "message: {$message} " ,LOG_DEBUG );
          $img_url = $picture;
          if( $object_id = @$data['object_id'] ){
            $img_url  = "https://graph.facebook.com/{$object_id}/picture";
          }
        }
        #return array();#XXXX
        $tmp = array(
                     'SocialPost' =>array(
                                          'social_account_id' => $social_account_id,
                                          'account_id' => $account_id,
                                          'type'       => 2 , #1:tw , 2:fb
                                          'post_id'    => $post_id,
                                          'title'      => $title,
                                          'body'       => $message,
                                          'original_posted' => $created,
                                          'is_posted'  => 0,
                                          'img_url'    => $img_url
                                          )
                     );
        $result[] = $tmp;
      }
      #print_r($result);
      #$this->log( print_r($result ,true)  ,LOG_DEBUG );
      $this->log( "[DEBUG] return facebook api count : " .count($result)  ,LOG_DEBUG );
      return $result;
    }
    $this->log("failure api call",LOG_DEBUG);
    return array();
  }
  function get_facebook_post_from_api_old($social_account_id,$account_id,$token,$since_time=null){
    $this->facebook = new Facebook( array(
      'appId' => '1424914961142418',//XXX
      'secret' => 'f89455c6ff04fb698e2e65f0296959ff',//XXX
      'cookie' => true
    ));
    
    $option = array('access_token' => $token);
    if($since_time){
      // $option['since'] = strtotime($since_time);
    }

    $feeds = $this->facebook->api("/me/posts",
                                  'GET',
                                  $option
    );

    $result = array();
    if( isset( $feeds['data'] )){
      foreach ( $feeds['data'] as $feed) {
        $tmp = $this->make_facebook_posts_data($feed);
        $tmp['SocialPost']['social_account_id'] = $social_account_id;
        $tmp['SocialPost']['account_id']       =  $account_id;
        $result[] = $tmp;
      }
    }
    return $result;
  }

  function get_facebook_post($social_account_id,$account_id,$token,$since_time=null){
    $this->log("[debug]get_facebook_post",LOG_DEBUG);
    if( Configure::read('Facebook.get_from_rss') ){
      return $this->get_facebook_post_from_rss($social_account_id,$account_id,$token,$since_time);
    }
    else{
      if(!$token){
        $this->log("token not found",LOG_DEBUG);
        return array();
      }
      # XXXTODO
      $this->log("[debug]get_facebook_post_from_api",LOG_DEBUG);
      $result = $this->get_facebook_post_from_api($social_account_id,$account_id,$token,$since_time);
      $this->log("[debug]get_facebook_post_from_api result count:" . count($result) ,LOG_DEBUG);
      return $result;
    }
  }

  function make_facebook_posts_data($feed){
    $ids = preg_split('/_/', $feed['id'] );
    $fb_user_id = $ids[0];
    $post_id    = $ids[1];
    $created_time = $feed['created_time'];
    //$body = "";
    $body = $this->make_facebook_body($feed);
    
    $data = array(
      'SocialPost' =>array(
                           //'social_account_id' => $account_id,
                           //'account_id' => $account_id,
                           'type'       => 2 , #1:tw , 2:fb
                           'post_id'    => $post_id,
                           'body'       => $body,
                           'original_posted' => $created_time,
                           'is_posted'  => 0
                           )
                  );
    return $data;
  }
  function make_facebook_body($feed){
    $body_parts = array();
    if(isset($feed['name']) ){
      $body_parts[] = $feed['name'];
    }
    if(isset($feed['link']) ){
      $body_parts[] = $feed['link'];
    }
    
    if(isset($feed['message']) ){
      $body_parts[] = $feed['message'];
    }
    //if(isset($feed['description']) ){
    //$body= $feed['description'];
    //}
    
    $body = join("\n",$body_parts);
    return $body;
  }


  function get_twitter_post2($screen_name,$max_id,$user_id){
    $this->log("[DEBUG] -- get_twitter_post2 --",LOG_DEBUG);
    $result =  $this->get_twitter_post($screen_name,$max_id,$user_id);
    $data = array();

    //error発生の場合からの配列を返却
    if( isset ( $result['error']) ){
      $this->log("[ERROR]".$result['error'] ,LOG_DEBUG);
      return $data;
    }

    $this->log("[DEBUG] ----- before result loop -------" ,LOG_DEBUG);
    foreach ($result as $tweet){
      $this->log("------ results loop -------" ,LOG_DEBUG);

      $created_at = strtotime( $tweet['created_at'] );
      $img_url = "";
      if(@$tweet['extended_entities']['media']){
        #$this->log( print_r($tweet['extended_entities']['media'] ,true)  ,LOG_DEBUG );
        $img_url = $tweet['extended_entities']['media'][0]['media_url'];
        $this->log( $img_url ,LOG_DEBUG );

        #foreach ($tweet['extended_entities']['media'] as $media){
        #  $this->log( $media['media_url'] ,LOG_DEBUG );
        #}
      }

      $data[] = array(
                      'SocialPost' =>array(
                                           'account_id' => $tweet['user']['screen_name'],
                                           'type'       => 1 , #1:tw , 2:fb
                                           'post_id'    => $tweet['id_str'],
                                           'body'       => $tweet['text'],
                                           'original_posted' => date('Y-m-d H:i:s',$created_at),
                                           'is_posted'  => 0,
                                           'img_url'    => $img_url,
                                           #'img_path'   => ""
                                           )
                      );
      # 画像が存在していれば保存処理XXXTODO0729
      # 'https://api.twitter.com/1.1/statuses/user_timeline.json',
    }
    #$this->log("RRRRRRRRRRRRRRRESULT"  ,LOG_DEBUG );
    #$this->log( print_r($data ,true)  ,LOG_DEBUG );

    return $data;
  }
  function twitter_oauth_instance($user_id){
    $social_setting = ClassRegistry::init('SocialSetting');
    $data =  $social_setting->find_by_user_id($user_id);

    if( isset( $data['SocialSetting'] )){
      $consumer_key    = $data['SocialSetting']['twitter_consumer_key'];
      $consumer_secret = $data['SocialSetting']['twitter_consumer_secret'];
      $access_token    =  $data['SocialSetting']['twitter_access_token'];
      $access_token_secret = $data['SocialSetting']['twitter_access_token_secret'];

      if(!$consumer_key || !$consumer_secret || !$access_token || !$access_token_secret){
        return;
      }

      $TwitterOAuth = new TwitterOAuth(
                                       $consumer_key,
                                       $consumer_secret,
                                       $access_token,
                                       $access_token_secret);
      return $TwitterOAuth;
    }

  }
  function get_twitter_post($screen_name,$max_id,$user_id){
    $this->log("[debug] get_twitter_post",LOG_DEBUG);
    $options = array(
                     'count'=>'20',
                     'screen_name' => $screen_name,
                     );
    if($max_id){
      $options['since_id'] = $max_id;
    }
    $options['exclude_replies'] =  'true';
    if ( $this->TwitterOAuth = $this->twitter_oauth_instance($user_id) ){
      $this->log("[debug] use TwitterOAuth ",LOG_DEBUG);
    
      $tw_response_json = $this->TwitterOAuth->OAuthRequest(
                                                            'https://api.twitter.com/1.1/statuses/user_timeline.json',
                                                            'GET',
                                                            $options
                                                            );
      $tw_response = json_decode($tw_response_json, true);

      if(!$tw_response ){ return array(); }
      elseif(isset($tw_response['errors'] ) ){
        return array();
      }
      //debug($tw_response);
      $tw_response = array_reverse($tw_response);//逆順にする
      return $tw_response;
    }
    return array();
  }
  function remove_saved_posts($posts,$saved_list){
    $new_posts = array();
    foreach ($posts as $p){
      $post_id = $p['SocialPost']['post_id'];
      if(!in_array($post_id,$saved_list )){
        $new_posts[] = $p;
      }
    }
    return $new_posts;
  }

  function twitter_user_check($screen_name,$user_id){
    $this->TwitterOAuth = $this->twitter_oauth_instance($user_id);
    if (!$this->TwitterOAuth){
      return "TW-ERR1";
    }

    $options = array(
                     'screen_name' => $screen_name,
                     );
    $tw_response_json = $this->TwitterOAuth->OAuthRequest(
      'https://api.twitter.com/1.1/users/show.json',
      'GET',
      $options
    );
    $tw_response = json_decode($tw_response_json, true);
    if(!$tw_response ){
      return "TW-ERR2";
    }
    elseif(isset($tw_response['errors'] ) ){
      return "TW-ERR3";
    }
    return $tw_response['id'];
  }

}