
<?php
App::uses('AppController', 'Controller');


class AdminController extends AppController {
  //読み込むコンポーネントの指定
  public $components = array('Session', 'Auth');
  public $uses = array( 'User');
  var $helpers = array('Html', 'Form', 'Csv'); 
  
  //どのアクションが呼ばれてもはじめに実行される関数
  public function beforeFilter()
  {
    parent::beforeFilter();
    //XXX
  }

  function download_csv() {
    Configure::write('debug', 0); // 警告を出さない
    $this->layout = false;
    $filename = 'user_' . date('YmdHis'); // 任意のファイル名
    #$genders = array('1' => '男性', '2' => '女性');
    $th = array('id', 'username',  ); // 表の一行目を作成
    $td = $this->User->find('all', array('fields' => $th)); //表の内容を取得
    $this->set(compact('filename', 'th', 'td'));
  }
  
  public function delete($id){
    #$this->User->id = $id;
    #$this->User->saveField('is_deleted',1);
    $data = array(  
      'User' => array(  
	'id' => $id,
	'is_deleted' => 1,  
      )  
    );  
    $this->User->save($data, false, array('is_deleted'));
    $this->Session->setFlash('ユーザーの削除を行いました');
    $this->redirect(array('action' => 'user'));
  }

  public function user(){
    if($this->request->is('post') || $this->request->is('put') ){
      foreach ( $this->request->data['User'] as $user){
	$this->User->save($user);
      }
      $this->Session->setFlash('入力完了');
    }
    $this->set('users', 
	       $this->User->find(
		 'all', 
		 array(
		   'conditions' => array(
		     'User.is_deleted' => 0 
		   )
		 )
	       )
    );
  }
  
}

