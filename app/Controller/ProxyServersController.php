<?php
App::uses('AppController', 'Controller');
/**
 * ProxyServers Controller
 *
 * @property ProxyServer $ProxyServer
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProxyServersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProxyServer->recursive = 0;
		$this->set('proxyServers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProxyServer->exists($id)) {
			throw new NotFoundException(__('Invalid proxy server'));
		}
		$options = array('conditions' => array('ProxyServer.' . $this->ProxyServer->primaryKey => $id));
		$this->set('proxyServer', $this->ProxyServer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProxyServer->create();
			if ($this->ProxyServer->save($this->request->data)) {
				$this->Session->setFlash(__('The proxy server has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proxy server could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProxyServer->exists($id)) {
			throw new NotFoundException(__('Invalid proxy server'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProxyServer->save($this->request->data)) {
				$this->Session->setFlash(__('The proxy server has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proxy server could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProxyServer.' . $this->ProxyServer->primaryKey => $id));
			$this->request->data = $this->ProxyServer->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProxyServer->id = $id;
		if (!$this->ProxyServer->exists()) {
			throw new NotFoundException(__('Invalid proxy server'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProxyServer->delete()) {
			$this->Session->setFlash(__('The proxy server has been deleted.'));
		} else {
			$this->Session->setFlash(__('The proxy server could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
