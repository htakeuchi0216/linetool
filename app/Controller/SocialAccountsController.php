<?php
App::uses('AppController', 'Controller');
/**
 * SocialAccounts Controller
 *
 * @property SocialAccount $SocialAccount
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SocialAccountsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SocialAccount->recursive = 0;
		$this->set('socialAccounts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SocialAccount->exists($id)) {
			throw new NotFoundException(__('Invalid social account'));
		}
		$options = array('conditions' => array('SocialAccount.' . $this->SocialAccount->primaryKey => $id));
		$this->set('socialAccount', $this->SocialAccount->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SocialAccount->create();
			if ($this->SocialAccount->save($this->request->data)) {
				$this->Session->setFlash(__('The social account has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social account could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SocialAccount->exists($id)) {
			throw new NotFoundException(__('Invalid social account'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SocialAccount->save($this->request->data)) {
				$this->Session->setFlash(__('The social account has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social account could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SocialAccount.' . $this->SocialAccount->primaryKey => $id));
			$this->request->data = $this->SocialAccount->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SocialAccount->id = $id;
		if (!$this->SocialAccount->exists()) {
			throw new NotFoundException(__('Invalid social account'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SocialAccount->delete()) {
			$this->Session->setFlash(__('The social account has been deleted.'));
		} else {
			$this->Session->setFlash(__('The social account could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
