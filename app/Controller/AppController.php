<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)

 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('TwitterOAuth', 'Vendor/twitteroauth/');
App::uses( 'CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
  public $layout = 'bootstrap';
  //
  //  public $helpers = array("Html", "Form", "TwitterBootstrap");
  public $helpers = array("Html", "Form");
#  public $components = array('Auth','RequestHandler');
  public $components =
    array(
      'Session',
      'Auth' => array(
	'loginRedirect' => array(
	  'controller' => 'posts',
	  'action' => 'index'
	),
	'logoutRedirect' => array(
	  'controller' => 'pages',
	  'action' => 'display',
	  'home'
	),
	'authenticate' => array(
	  'Form' => array(
	    'passwordHasher' => 'Blowfish'
	  )
	)
      )
    );
  public $uses = array( 'User' , 'SocialSetting');

  public function sendMail($title,$body){
    $email = new CakeEmail('gmail');

    $mail_to   = Configure::read('ErrMailTo');
    $mail_from = Configure::read('MailFrom');
    $res = $email->config(array('log' => 'emails'))
      ->from(array($mail_from))
      ->to( $mail_to )
      ->subject( $title )
      ->send( $body );
  }

  public function beforeFilter() {
    if( $maintenance_flg = "0" ){#メンテナンス中表示箇所
      print "只今システムメンテナンス中です。<br>ご迷惑をおかけしますが今しばらくお待ちください。";
      exit();
    }

    $this->set('user', $this->Auth->user());
    if($user_id = $this->Auth->user('id')){
      $social_setting = $this->SocialSetting->find_by_user_id($user_id);
      $this->set('social_setting', $social_setting );
    }
    #$this->basic_auth();
  }

  protected function basic_auth($loginId = 'ajtja'){
    //Basic認証　
    //    $this->autoRender = false;
 
    //$loginId = 'hoge';
    $loginPassword = '2015dmwmd06';
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
      header('WWW-Authenticate: Basic realm="Please enter your ID and password"');
      header('HTTP/1.0 401 Unauthorized');
      die("id / password Required");
    } else {
      if ($_SERVER['PHP_AUTH_USER'] != $loginId || $_SERVER['PHP_AUTH_PW'] != $loginPassword) {
        header('WWW-Authenticate: Basic realm="Please enter your ID and password"');
        header('HTTP/1.0 401 Unauthorized');
        die("Invalid id / password combination.  Please try again");
      }
    }
  }

  
}
