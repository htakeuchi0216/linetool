<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

/**
 * To prefer app translation over plugin translation, you can set
 *
 * Configure::write('I18n.preferApp', true);
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
        'mask'  => 0666, 
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
        'mask' => 0666, 
));
CakePlugin::load('DebugKit');
CakePlugin::load(array('TwitterBootstrap'));



define('CONSUMER_KEY',        'ujtAZ01C7es97fLSwenew');
define('CONSUMER_SECRET',     'KQKMVPNI9f6FqGC5KLG4szs0BVgTYwudMte9VgvOwc0');
define('ACCESS_TOKEN',        '106943820-2iSBnGl91NbS19KUC6xQNkTFz7GDqj7WXy39YO9g');
define('ACCESS_TOKEN_SECRET', 'AXWtliP6FLOtFmyVU4epWx9mJ6VlmXgJujA33nEcDL8');

CakePlugin::load('Composer', array('bootstrap' => true));

//app/Config/bootstrap.php
CakePlugin::load('Facebook');


//http://shigurui.kirscheweb.com/?p=136
/**
 * 認証用プラグイン
 */
CakePlugin::load('Opauth', array('routes' => true, 'bootstrap' => true));
 
Configure::write('Opauth.callback_url', Configure::read('Opauth.path').'callback');
/**
 * Facebook認証のためのstrategy
 */
Configure::write('Opauth.Strategy.Facebook', array(
  'app_id'     => '1424914961142418',
  'app_secret' => 'f89455c6ff04fb698e2e65f0296959ff',
  'scope'      => 'read_stream'
#  'app_id' => '115295265202658',
#  'app_secret' => '46772b9d018a2f7fe01948ef3dd2d63c'
));
 
/**
 * 認証用のURL設定
 */
Configure::write('Opauth.path', '/auth/');
Configure::write('Opauth._cakephp_plugin_complete_url', '/hogehoge');
/*
 * Twitter
*/

//Configure::write('Opauth.Strategy.Twitter', array(
//  'key'    => 'あなたのAPI key',
//  'secret' => 'あなたのAPI secret'
//));


/**
 * Configures default file logging options
 */
App::uses( 'CakeLog', 'Log');
CakeLog::config( 'debug', array(
  'engine' => 'FileLog',
  'types' => array( 'notice', 'info', 'debug'),
  'file' => date( 'Ymd') . '_' . 'debug',       // 変更
  //  'size' => ‘5MB’                           // 5Mごとにファイル切替
  //  'rotate' => 20                            // 20ファイルまで切替
   'mask' => 0666, 
));
CakeLog::config( 'error', array(
  'engine' => 'FileLog',
  'types' => array( 'warning', 'error', 'critical', 'alert', 'emergency'),
  'file' => date( 'Ymd') . '_' . 'error',       // 変更
  'mask' => 0666, 
));

Configure::write('Security.db.key', '8QYJFYKbeJ2GtQXBtftgWsa5p76MhQXx');
if ( !defined('MCRYPT_RIJNDAEL_256') ) {
  define('MCRYPT_RIJNDAEL_256', 0);
}
if ( !defined('MCRYPT_MODE_CBC') ) {
  define('MCRYPT_MODE_CBC', 0);
}
