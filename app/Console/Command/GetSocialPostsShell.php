<?php

App::uses('ComponentCollection', 'Controller');
App::uses('SocialCommonComponent', 'Controller/Component'); 
//Console/cake postToLine
class GetSocialPostsShell extends AppShell {
  public $uses = array("User","SocialAccount","SocialPost");

  public function p($str){
    $this->out($str);
  }

  public function dlog($str){
    $this->log("[BATCH]".$str,LOG_DEBUG);
  }

  public function startup() {
    $collection = new ComponentCollection();
    $this->SocialCommon = new SocialCommonComponent($collection);
    parent::startup();
  }

  public function get_file_type($http_response_header){
    $img_mine_types = array(
                            'jpg' => "image/jpeg",
                            'png' => "image/png",
                            'gif' => "image/gif",
                            );
    foreach ($http_response_header as $key => $val){
      if(preg_match("/Content-Type:/i",$val) ){
        foreach($img_mine_types as $filetype => $mine_type){
          $this->dlog("[DEBUG] check filetype $filetype ");
          if(preg_match("|$mine_type|i",$val) ){
          $this->dlog("[DEBUG] MATCH! {$filetype} ");
            return $filetype;
          }
        }
        $this->dlog("[DEBUG] do not match Content-Type: ");
        break;
      }
    }
    // 特定できなければjpgとする
    return "jpg";
  }

  public function download_image(){
    $this->dlog("##############################");
    $this->dlog("###START download_image ######");
    $have_img_posts = $this->SocialPost->get_have_img_posts($account_id,$tw);
    $this->dlog( print_r($have_img_posts, true) );
    $output_path = "/var/www/html/cakephp/app/images/";
    foreach ($have_img_posts as $p ){
      $url = $p['social_posts']['img_url'];
      $id  = $p['social_posts']['id'];
      $this->dlog(" :: ". $url );
      if( $data = file_get_contents($url) ){
        $this->dlog(" XXX image download success ($id)");
        #$this->dlog(print_r($data,true ));
        $filetype = $this->get_file_type($http_response_header);
        $output_fullpath = $output_path . $id. ".". $filetype;
        $this->dlog(" [debug] output to :$output_fullpath ");
        file_put_contents($output_fullpath,$data);
        #print "===================";
        #var_dump($http_response_header);
        # UPDATE social_posts SET img_download_flg = 1 WHERE id = ?
         // 更新
        $data = array('SocialPost' => array('id' => $id, 'img_download_flg' => 1));
        $fields = array('img_download_flg');
        $this->SocialPost->save($data, false, $fields);
      }
      else{
        $this->dlog(" XXX image download failure ($url)");
        # UPDATE social_posts SET img_download_flg = 9 WHERE id = ?
        $data = array('SocialPost' => array('id' => $id, 'img_download_flg' => 9));
        $fields = array('img_download_flg');
        $this->SocialPoste->save($data, false, $fields);
      }
    }

  }
  public function main() {
    $indent = "  ";
    $users = $this->User->getAvailables();
    $this->dlog("##############################");
    $this->dlog("##START OF GetSocialPostsSell##");
    foreach ($users as $user ){
      $user_id = $user['User']['id'];
      $this->dlog( "User_id : $user_id");
      $social_accounts =$this->SocialAccount->get_by_user_id( $user_id);

      if(empty($social_accounts)){
        $this->dlog($indent."NO ACCOUNT DATA");
      }

      foreach($social_accounts as $account){
        $data = $account['SocialAccount'];
        $account_id = $data['id'];
        $fb       = $data['facebook_account_id'];
        $fb_token = $data['facebook_token'];
        $tw = $data['twitter_account_id'];
        
        $tw_max_id = $this->SocialPost->get_twitter_max_post_id($account_id,$tw);
        $fb_max_id = $this->SocialPost->get_facebook_max_post_id($account_id,$fb);
        
        $this->dlog($indent." account_id : $account_id ");
        $this->dlog($indent." GET FROM $fb(facebook) , $tw(twitter) ");
        //$this->p($indent."get_twitter_post($tw, $tw_max_id )");
        //$this->p($indent."get_facebook_post($account_id,$fb,".substr($fb_token,0,4) . "..)" );//XXXsince_date

        $twitter_posts  = $this->SocialCommon->get_twitter_post2($tw,$tw_max_id,$user_id);
        $facebook_posts = $this->SocialCommon->get_facebook_post($account_id,$fb,$fb_token);

        $this->dlog($indent." twitter_posts before:". count($twitter_posts) );
        $this->dlog($indent." facebook_posts before:". count($facebook_posts) );

        #$this->dlog("XXXX DUMP (POSTS) XXX" );
        #$this->dlog( print_r($twitter_posts,true ), LOG_DEBUG );
        #$this->dlog( print_r($facebook_posts,true ) , LOG_DEBUG);
        #$this->dlog("");

# 取得してきたデータのうち、すでに保存済みのものがあれば取り除く
        $twitter_saved_list   = $this->SocialPost->get_saved_post_ids($account_id,1);
        $facebook_saved_list  = $this->SocialPost->get_saved_post_ids($account_id,2);
        $twitter_posts  = $this->SocialCommon->remove_saved_posts($twitter_posts ,$twitter_saved_list);
        $facebook_posts = $this->SocialCommon->remove_saved_posts($facebook_posts,$facebook_saved_list);

        $this->dlog($indent." twitter_posts after:". count($twitter_posts) );
        $this->dlog($indent." facebook_posts after:". count($facebook_posts) );

        $posts = array_merge($twitter_posts, $facebook_posts);
        foreach($posts as $p ){
          $data = $p['SocialPost'];
          $data['social_account_id'] = $account_id;
          $this->SocialPost->create();
          $ret = $this->SocialPost->save($data);
          $this->dlog( "@@@@@@@@@@@@@ DO SAVE SocialPost @@@@@@@@@@@@@@@@@@@@" );
          #$this->dlog( print_r($data, true) );
          if($ret){
            $this->dlog($indent." save success ? " );
          }else{
            #$this->dlog( print_r($this->SocialPost->validationErrors, true) );
            $this->dlog($indent." save failure ? " );
          }
        }
      }
    }

    $this->download_image();
  }

}