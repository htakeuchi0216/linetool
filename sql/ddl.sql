-- MySQL dump 10.13  Distrib 5.5.42, for Linux (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `social_accounts`
--

DROP TABLE IF EXISTS `social_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `seq` int(11) DEFAULT '1',
  `line_account_id` varchar(128) DEFAULT NULL,
  `twitter_account_id` varchar(128) DEFAULT NULL,
  `facebook_account_id` varchar(128) DEFAULT NULL,
  `facebook_num_id` bigint(20) DEFAULT NULL,
  `facebook_display_name` varchar(128) DEFAULT NULL,
  `facebook_token` varchar(256) DEFAULT NULL,
  `facebook_token_expires` datetime DEFAULT NULL,
  `is_line_available` tinyint(1) DEFAULT '0',
  `is_twitter_available` tinyint(1) DEFAULT '0',
  `is_facebook_available` tinyint(1) DEFAULT '0',
  `line_available_datetime` datetime DEFAULT NULL,
  `twitter_available_datetime` datetime DEFAULT NULL,
  `facebook_available_datetime` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_posts`
--

DROP TABLE IF EXISTS `social_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `social_account_id` int(11) NOT NULL,
  `account_id` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `body` mediumtext,
  `original_posted` datetime NOT NULL,
  `is_posted` tinyint(1) DEFAULT '0',
  `line_posted` datetime DEFAULT NULL,
  `img_url` text,
  `img_download_flg` int(1) default 0,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_account_id` (`social_account_id`,`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2312 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/* 20150731 
ALTER TABLE social_posts ADD COLUMN 'img_url' text;
ALTER TABLE social_posts ADD COLUMN img_download_flg int(1) default 0;
*/

--
-- Table structure for table `social_setting`
--

DROP TABLE IF EXISTS `social_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `facebook_access_token` varchar(255) DEFAULT NULL,
  `facebook_api_key` varchar(255) DEFAULT NULL,
  `facebook_api_secret` varchar(255) DEFAULT NULL,
  `twitter_consumer_key` varchar(255) DEFAULT NULL,
  `twitter_consumer_secret` varchar(255) DEFAULT NULL,
  `twitter_access_token` varchar(255) DEFAULT NULL,
  `twitter_access_token_secret` varchar(255) DEFAULT NULL,
  `line_login_id` varchar(255) DEFAULT NULL,
  `line_login_password` varchar(255) DEFAULT NULL,
  `line_confirmation_code` varchar(255) DEFAULT NULL,
  `line_status` int(11) DEFAULT '0',
  `line_login_ng_time` datetime DEFAULT NULL,
  -- cookie
  -- `user_agent` varchar(255) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_display_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `line_account_limit` int(11) DEFAULT '1',
  `is_admin` int(11) DEFAULT '0',
  `is_deleted` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


-- status
--  0: 通常状態
--  1: 制限状態
DROP TABLE IF EXISTS `proxy_server`;
CREATE TABLE `proxy_server` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip`   varchar(50) DEFAULT NULL,
  `port` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `area`     varchar(50) DEFAULT NULL,
  `postcount` int(11)  DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `disabled_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dump completed on 2015-04-04 12:42:19

