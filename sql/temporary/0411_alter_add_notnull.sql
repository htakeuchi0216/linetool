-- 04/11追加
-- social_setting.user_id 
ALTER TABLE social_setting MODIFY COLUMN `user_id` varchar(50) NOT NULL;

-- social_accounts.user_id
ALTER TABLE social_accounts MODIFY COLUMN `user_id` varchar(50) NOT NULL;

-- social_posts.account_id
-- social_posts.post_id
-- social_posts.original_posted
ALTER TABLE social_posts MODIFY COLUMN `account_id` varchar(64) NOT NULL;
ALTER TABLE social_posts MODIFY COLUMN `post_id` bigint(20) NOT NULL;
ALTER TABLE social_posts MODIFY COLUMN `original_posted` datetime NOT NULL;
