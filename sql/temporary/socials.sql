CREATE TABLE social_accounts (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id VARCHAR(50),
    seq     integer DEFAULT 1,
    line_account_id VARCHAR(128),
    twitter_account_id VARCHAR(128),
    facebook_account_id VARCHAR(128),
    facebook_num_id BIGINT,
    facebook_display_name VARCHAR(128) DEFAULT NULL,
    facebook_token VARCHAR(256) DEFAULT NULL,
    facebook_token_expires DATETIME DEFAULT NULL,
    is_line_available tinyint(1) DEFAULT 1,
    is_twitter_available tinyint(1) DEFAULT 1,
    is_facebook_available tinyint(1) DEFAULT 1,

    line_available_datetime DATETIME DEFAULT NULL,
    twitter_available_datetime DATETIME DEFAULT NULL,
    facebook_available_datetime DATETIME DEFAULT NULL,
    is_deleted integer DEFAULT 0,
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
);
-- 03/21追加
--ALTER TABLE social_accounts ALTER is_available SET DEFAULT 1 ;
--ALTER TABLE social_accounts CHANGE  is_available is_line_available tinyint(1);
--ALTER TABLE social_accounts ADD COLUMN is_twitter_available  tinyint(1) DEFAULT 1 AFTER is_line_available;
--ALTER TABLE social_accounts ADD COLUMN is_facebook_available tinyint(1) DEFAULT 1 AFTER is_twitter_available;

-- 03/22追加
--ALTER TABLE social_accounts ADD COLUMN line_available_datetime     DATETIME DEFAULT NULL AFTER is_facebook_available;
--ALTER TABLE social_accounts ADD COLUMN twitter_available_datetime  DATETIME DEFAULT NULL AFTER line_available_datetime;
--ALTER TABLE social_accounts ADD COLUMN facebook_available_datetime DATETIME DEFAULT NULL AFTER twitter_available_datetime;

-- 03/24追加
-- ALTER TABLE social_accounts ADD COLUMN facebook_display_name VARCHAR(128) DEFAULT NULL AFTER facebook_account_id;
-- ALTER TABLE social_accounts ADD COLUMN facebook_token        VARCHAR(256) DEFAULT NULL AFTER facebook_display_name;
-- ALTER TABLE social_accounts ADD COLUMN facebook_token_expires DATETIME    DEFAULT NULL AFTER facebook_token;

-- 03/28追加
ALTER TABLE social_accounts ADD COLUMN facebook_num_id BIGINT  DEFAULT NULL AFTER facebook_account_id;


-- CREATE TABLE user_line (
--    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
--    user_id VARCHAR(50),
--    seq     integer DEFAULT 1,
--    line_account_id varchar(64),
--    is_available integer DEFAULT 0,
--    is_deleted integer DEFAULT 0,
--    created DATETIME DEFAULT NULL,
--    modified DATETIME DEFAULT NULL
--);
--ALTER TABLE user_line ADD UNIQUE (user_id,seq);

-- type: 1:twitter , 2:faceboo
--CREATE TABLE social_account (
--    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
--    user_line_id VARCHAR(50),
--    type integer NOT NULL,
--    social_account_id VARCHAR(128),
--    is_available integer DEFAULT 0,
--    is_deleted integer DEFAULT 0,
--    created DATETIME DEFAULT NULL,
--    modified DATETIME DEFAULT NULL
--);
--ALTER TABLE social_account ADD UNIQUE (user_line_id,type);

-- type 1: twitter , 2:facebook
-- account:それぞれのsocialでのID,
--   social_accountsのものと同一だが、変更された場合を想定してログとしてこのテーブルにも残す
CREATE TABLE social_posts (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    social_account_id INT NOT NULL,
    account_id varchar(64),
    type integer NOT NULL,
    post_id  BIGINT NOT NULL,
    title varchar(64),
    body text,
    is_posted boolean DEFAULT false,
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
);
ALTER TABLE social_posts ADD UNIQUE (social_account_id,post_id);
-- 3/2X 追加
--ALTER TABLE social_post CHANGE  post_id post_id BIGINT;
-- 3/24オリジナル投稿日付、LINE投稿日付を追加
--ALTER TABLE social_posts ADD COLUMN original_posted DATETIME DEFAULT NULL AFTER body;
--ALTER TABLE social_posts ADD COLUMN line_posted     DATETIME DEFAULT NULL AFTER is_posted;

-- 3/22 テーブル文字コード修正
alter table posts charset=utf8;
alter table social_accounts charset=utf8;
alter table social_posts charset=utf8;
alter table social_setting charset=utf8;
alter table user_line charset=utf8;
alter table users charset=utf8;

ALTER TABLE posts CONVERT TO CHARACTER SET utf8;
ALTER TABLE social_accounts CONVERT TO CHARACTER SET utf8;
ALTER TABLE social_posts CONVERT TO CHARACTER SET utf8;
ALTER TABLE social_setting  CONVERT TO CHARACTER SET utf8;
ALTER TABLE users CONVERT TO CHARACTER SET utf8;
