-- 03/29追加
ALTER TABLE social_setting DROP COLUMN twitter_access_token;
ALTER TABLE social_setting DROP COLUMN twitter_api_key;
ALTER TABLE social_setting DROP COLUMN twitter_api_secret;

--CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET
ALTER TABLE social_setting ADD COLUMN twitter_consumer_key    VARCHAR(255) DEFAULT NULL AFTER facebook_api_secret;
ALTER TABLE social_setting ADD COLUMN twitter_consumer_secret VARCHAR(255) DEFAULT NULL AFTER twitter_consumer_key;
ALTER TABLE social_setting ADD COLUMN twitter_access_token    VARCHAR(255) DEFAULT NULL AFTER twitter_consumer_secret;
ALTER TABLE social_setting ADD COLUMN twitter_access_token_secret VARCHAR(255) DEFAULT NULL AFTER twitter_access_token;

