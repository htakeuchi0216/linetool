-- 03/24追加
-- ALTER TABLE social_accounts ADD COLUMN facebook_display_name VARCHAR(128) DEFAULT NULL AFTER facebook_account_id;
-- ALTER TABLE social_accounts ADD COLUMN facebook_token        VARCHAR(256) DEFAULT NULL AFTER facebook_display_name;
-- ALTER TABLE social_accounts ADD COLUMN facebook_token_expires DATETIME    DEFAULT NULL AFTER facebook_token;

-- 3/24オリジナル投稿日付、LINE投稿日付を追加
ALTER TABLE social_posts ADD COLUMN original_posted DATETIME DEFAULT NULL AFTER body;
ALTER TABLE social_posts ADD COLUMN line_posted     DATETIME DEFAULT NULL AFTER is_posted;
