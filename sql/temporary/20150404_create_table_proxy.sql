
-- status
--  0: 通常状態
--  1: 制限状態
CREATE TABLE `proxy_server` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip`   varchar(50) DEFAULT NULL,
  `port` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `area`     varchar(50) DEFAULT NULL,
  `postcount` int(11)  DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `disabled_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
