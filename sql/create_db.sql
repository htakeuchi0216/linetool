CREATE DATABASE IF NOT EXISTS `line` DEFAULT CHARACTER SET utf8;

-- SET PASSWORD FOR root@localhost=PASSWORD('2015root040x');
GRANT ALL ON line.* to lineuser@localhost;
FLUSH PRIVILEGES;
SET PASSWORD FOR lineuser@localhost=password('lineuser');
